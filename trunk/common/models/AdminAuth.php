<?php

namespace Qbon\Models;

class AdminAuth extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array('flag_active'));
       $this->skipAttributesOnUpdate(array('create_time'));
    }
}
