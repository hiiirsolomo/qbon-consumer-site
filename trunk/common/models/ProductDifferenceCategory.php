<?php

namespace Qbon\Models;

class ProductDifferenceCategory extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array('flag_active'));
       $this->skipAttributesOnUpdate(array('create_time'));
       $this->belongsTo("product_property_category_id",
                        __NAMESPACE__ . "\\" . "ProductPropertyCategory",
                        "product_property_category_id",
                        array('alias'=>'ProductPropertyCategory'));
    }
}
