<?php

namespace Qbon\Models;

class Staff extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array("create_time"));
       $this->skipAttributesOnUpdate(array("create_time"));
    }
}
