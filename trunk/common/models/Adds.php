<?php

namespace Qbon\Models;

class Adds extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array('flag_active'));
       $this->useDynamicUpdate(true);
    }
}
