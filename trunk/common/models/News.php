<?php
/**
* Class and Function List:
* Function list:
* - initialize()
* Classes list:
* - News extends \
*/
namespace Qbon\Models;

class News extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
        $this->skipAttributesOnCreate(array(
            'flag_active',
            'create_time'
        ));
        $this->useDynamicUpdate(true);
    }
}
