<?php

namespace Qbon\Models;

class ProductPropertyCategory extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array('flag_active'));
       $this->skipAttributesOnUpdate(array('create_time'));
    }
}
