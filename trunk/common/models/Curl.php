<?php
/**
 * Class and Function List:
 * Function list:
 * - initialize()
 * - getResource()
 * Classes list:
 * - Curl extends \
 */
namespace Qbon\Models;
class Curl extends \Phalcon\Mvc\Model
{
    
    public function initialize()
    {
    }
    public function getResource($url, $token, $postArray = NULL)
    {
        $ch = curl_init();
        
        //永遠抓最新
        $header[] = "Cache-Control: no-cache";
        $header[] = "Pragma: no-cache";
        $header[] = "APPLICATION-KEY:" . API_KEY;
        $header[] = "AUTHORIZATION:" . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        //等待時間
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        
        //Post Data
        if ($postArray) {
            
            $postData = http_build_query($postArray);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }
        
        // 設定referer
        curl_setopt($ch, CURLOPT_REFERER, API_URL);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($result);
        return $obj;
    }
}
