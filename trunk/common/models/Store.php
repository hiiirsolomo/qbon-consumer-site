<?php

namespace Qbon\Models;

class Store extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
       $this->skipAttributesOnCreate(array("create_time"));
       $this->skipAttributesOnUpdate(array("create_time"));
    }
}
