<?php

	$di = new \Phalcon\DI\FactoryDefault();

	// Registering a router
	$di->set("router", require __DIR__."/routes.php");

	//Set the views cache service
	$di->set("viewCache", function(){

		//Cache data for one day by default
		$frontCache = new Phalcon\Cache\Frontend\Output(array(
			// "lifetime" => 86400
			"lifetime" => 0
		));

		//File backend settings
		$cache = new Phalcon\Cache\Backend\File($frontCache, array(
			"cacheDir" => __DIR__."/../../var/cache/",
		));

		return $cache;
	});


	// Main logger file
	$di->set("logger", function() {
		return new \Phalcon\Logger\Adapter\File(__DIR__."/../../var/logs/".date('Y-m-d').".log");
	}, true);


	// Register the flash service with custom CSS classes
	$di->set("flash", function() {
		return new Phalcon\Flash\Direct(array(
			"error" => "alert alert-danger",
			"success" => "alert alert-success",
			"notice" => "alert alert-info",
		));
	});

	// Register the flash service with custom CSS classes
	$di->set("flashSession", function() {
		return new Phalcon\Flash\Session(array(
			"error" => "alert alert-danger",
			"success" => "alert alert-success",
			"notice" => "alert alert-info",
		));
	});

	// event
	$di->set('eventsManager', new \Phalcon\Events\Manager(), true);


	// Database connection is created based in the parameters defined in the configuration file
	$di->set("db", function() use ($di) {
		$dbConfig = require __DIR__."/db.php";
		$connection = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
			"host" => $dbConfig->database->host,
			"username" => $dbConfig->database->username,
			"password" => $dbConfig->database->password,
			"dbname" => $dbConfig->database->name,
			"charset" => "utf8",
            "options" => Array(
    			PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,  //開啟錯誤throw exception
    			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `UTF8`',
    			PDO::ATTR_TIMEOUT=>5)
		));
		if (ENV == "development") {

		    $profiler = new \Phalcon\Db\Profiler();
		    $eventsManager = $di->get("eventsManager");

		    //$eventsManager->collectResponses(true);
		    $eventsManager->attach("db", function($event, $connection) use ($profiler){
		        if ($event->getType() == "beforeQuery") {
		            $profiler->startProfile($connection->getSQLStatement());
		            // echo $connection->getSQLStatement() . "<br/>";

		        }
		        if ($event->getType() == "afterQuery") {

		            $profiler->stopProfile();
		        }
		        // echo "REAL :: ". $connection->getRealSQLStatement() . "<br/>";

		    });
		    $connection->setEventsManager($eventsManager);
		}
		return $connection;
	});

	$di->set("filter", function() {
		$filter = new \Phalcon\Filter();

		//Using an anonymous function
		$filter->add("sqlInjectionAnti", function($value) {
			return \Mike\Util::sqlInjectionAnti($value);
		});

		return $filter;
	});

	//loader
	$loader = new \Phalcon\Loader();

	$loader->registerNamespaces(array(
		"Qbon\Models" => __DIR__."/../models/",
		"Mike" => __DIR__ . '/../library/',
	))->registerDirs(array(
			__DIR__ . '/../library/',
			__DIR__ . '/../plugins/'
		)
	)->registerClasses(
	    array(
	        "PasswordHash" => LIB_ROOT . "/phpass/PasswordHash.php",
	    )
	)->register();


	$di->set('dispatcher', function() use ($di) {
		$dispatcher = new Phalcon\Mvc\Dispatcher();

		$eventsManager = $di->getShared('eventsManager');

		$paramHandler = new \ParamHandler($di);
		$eventsManager->attach('dispatch', $paramHandler);

		$dispatcher->setEventsManager($eventsManager);
		return $dispatcher;
	});


	// Error handler
	set_error_handler(function($errno, $errstr, $errfile, $errline) use ($di)
	{

		if (!(error_reporting() & $errno)) {
			return;
		}

		$di->getFlash()->error($errstr);
		$di->getLogger()->log($errstr.' '.$errfile.':'.$errline, Phalcon\Logger::ERROR);

		return true;
	});
