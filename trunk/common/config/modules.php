<?php

return array(
	'frontend' => array(
		'className' => 'Qbon\Frontend\Module',
		'path' => '../apps/frontend/Module.php'
	),
	'backend' => array(
	        'className' => 'Qbon\Backend\Module',
        	'path' => '../apps/backend/Module.php'
    	),
    	'mobile' => array(
		'className' => 'Qbon\Mobile\Module',
		'path' => '../apps/mobile/Module.php'
	)
);
