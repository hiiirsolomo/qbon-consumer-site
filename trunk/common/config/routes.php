<?php
/**
 * Class and Function List:
 * Function list:
 * Classes list:
 */
$router = new \Phalcon\Mvc\Router();

$router->setDefaultModule("frontend");

/**
 * Frontend routes
 */

$router->add('/', array(
    'module' => 'frontend',
    'namespace' => 'Qbon\Frontend\Controllers\\',
));

$router->add("/:controller(/?)", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => 1,
    "action" => "index"
));

$router->add("/:controller/:action/:params", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => 1,
    "action" => 2,
    "params" => 3
));


$router->add("/news/:params", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => "news",
    "action" => "index",
    "params" => 1
));

$router->add("/wallet/:params", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => "wallet",
    "action" => "index",
    "params" => 1
));

$router->add("/apps/:params", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => "apps",
    "action" => "index",
    "params" => 1
));

$router->add("/signout", array(
    "module" => "frontend",
    "namespace" => "Qbon\Frontend\Controllers\\",
    "controller" => "index",
    "action" => "signout"
));


/**
 * Ｍobile routes
 */

$router->add("/mobile(/?)", array(
    "module" => "mobile",
    "namespace" => "Qbon\Mobile\Controllers\\",
    "controller" => 'index',
    "action" => "index",
));

$router->add("/mobile/:controller/:action/:params", array(
    "module" => "mobile",
    "namespace" => "Qbon\Mobile\Controllers\\",
    "controller" => 1,
    "action" => 2,
    "params" => 3
));

$router->add("/mobile/:controller(/?)", array(
    "module" => "mobile",
    "namespace" => "Qbon\Mobile\Controllers\\",
    "controller" => 1,
    "action" => "index"
));

/**
 * Backend routes
 */
$router->add("/admin(/?)", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => 'index',
    "action" => "index",
));

$router->add("/admin/:controller/:action/:params", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => 1,
    "action" => 2,
    "params" => 3
));

$router->add("/admin/:controller(/?)", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => 1,
    "action" => "index"
));

$router->add("/admin/signin", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => "index",
    "action" => "signin"
));

$router->add("/admin/signout", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => "index",
    "action" => "signout"
));

$router->add("/admin/signup", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => "index",
    "action" => "signup"
));

$router->add("/admin/:controller/:action/:params", array(
    "module" => "backend",
    "namespace" => "Qbon\Backend\Controllers\\",
    "controller" => 1,
    "action" => 2,
    "params" => 3
));


return $router;
