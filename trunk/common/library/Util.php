<?php

/*
*  工具 function
*/

namespace Mike;

class Util
{
    public static function MIMETypeToExt($mimeType)
    {
        switch($mimeType) {
            case "image/jpeg":
                $ext = '.jpg';
                break;
            case "image/png":
                $ext = '.png';
                break;
            case "image/gif":
                $ext = '.gif';
                break;
            default:
                $ext = '';
        }
        return $ext;
    }

	//ARRAY排序，可針對ARRAY中的某個欄位做排序
	public static function stickySort($arr, $field, $sort_type, $sticky_fields = array())
	{
		$i = 0;
		if (sizeof($arr))
			foreach ($arr as $value)
			{
				$is_contiguous = true;
				if (!empty($grouped_arr))
				{
					$last_value = end($grouped_arr[$i]);

					if (!($sticky_fields == array()))
					{
						foreach ($sticky_fields as $sticky_field)
						{
							if ($value[$sticky_field] <> $last_value[$sticky_field])
							{
								$is_contiguous = false;
								break;
							}
						}
					}
				}
				if ($is_contiguous)
					$grouped_arr[$i][] = $value;
				else
					$grouped_arr[++$i][] = $value;
			}
		$code = '';
		switch ($sort_type)
		{
			case "ASC_AZ":
				$code .= 'return strcasecmp($a["' . $field . '"], $b["' . $field . '"]);';
				break;
			case "DESC_AZ":
				$code .= 'return (-1*strcasecmp($a["' . $field . '"], $b["' . $field . '"]));';
				break;
			case "ASC_NUM":
				$code .= 'return ($a["' . $field . '"] > $b["' . $field . '"]);';
				break;
			case "DESC_NUM":
				$code .= 'return ($b["' . $field . '"] > $a["' . $field . '"]);';
				break;
		}

		$compare = create_function('$a, $b', $code);

		if (sizeof($grouped_arr))
			foreach ($grouped_arr as $grouped_arr_key => $grouped_arr_value)
				usort($grouped_arr[$grouped_arr_key], $compare);

		$arr = array();
		if (sizeof($grouped_arr))
			foreach ($grouped_arr as $grouped_arr_key => $grouped_arr_value)
				foreach ($grouped_arr[$grouped_arr_key] as $grouped_arr_arr_key => $grouped_arr_arr_value)
					$arr[] = $grouped_arr[$grouped_arr_key][$grouped_arr_arr_key];

		return $arr;
	}

	//Hiiir Hero Log
	public static function hiiirLog($log){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_URL, "http://log.hiiir.com/tracklog.php?Val=".$log);
        //curl_setopt ($ch,CURLOPT_SSLVERSION, 3);
        curl_exec ($ch);
        if(curl_errno($ch)){
            $Json['error'] = 'connection error';
            $resultcode = json_encode($Json);
        }
    	//關閉Curl釋放記憶體
        curl_close($ch);
    }

    /**
     * 用來格式化輸出的部份，避免內嵌js語法
     *
     * @param String $text
     * @return 格式化後的$text
     */
    public static function formatTxt($text) {
        $text = str_replace( "$", "", $text);
        return htmlspecialchars(strip_tags(stripslashes(trim($text))));
    }

    /**
     * 將BIG5的文字轉換成UTF8
     *
     * @param String $msg
     * @return 轉換後的文字
     */
    public static function BIG52UTF($msg) {
        $result = iconv("big5","UTF-8//TRANSLIT",$msg);
        return trim($result);
    }
    /**
     * 將UTF-8的文字轉換成BIG5
     *
     * @param String $msg
     * @return 轉換後的文字
     */
    public static function UTF2BIG($msg) {
        $result = iconv("UTF-8","big5//TRANSLIT",$msg);
        return trim($result);
    }

    public static function tranceHtmlEntities($str){
        $trans = get_html_translation_table(HTML_ENTITIES);
        //$encoded = strtr($str, $trans);
        $trans = array_flip($trans);
        return strtr($str, $trans);
    }

    //註： 網路上的回應幾乎是 with PDOs is more good but this solution for simple website is enough i think
    public static function sqlInjectionAnti($str){
        if (sprintf("%d",$str) == $str) {
            return $str;
        }
        $trance_str = self::tranceHtmlEntities($str);
        $trance_str = str_replace(array('\\"',"\\'"), array('"',"'"), $trance_str);
        $new_str = mysql_escape_string($trance_str);
        return $new_str;
    }

    public static function is_email($email, $checkDNS = false) {
        //  Check that $email is a valid address
        //      (http://tools.ietf.org/html/rfc3696)
        //      (http://tools.ietf.org/html/rfc5322#section-3.4.1)
        //      (http://tools.ietf.org/html/rfc5321#section-4.1.3)
        //      (http://tools.ietf.org/html/rfc4291#section-2.2)
        //      (http://tools.ietf.org/html/rfc1123#section-2.1)

        //  Contemporary email addresses consist of a "local part" separated from
        //  a "domain part" (a fully-qualified domain name) by an at-sign ("@").
        //      (http://tools.ietf.org/html/rfc3696#section-3)
        $index = strrpos($email,'@');

        if ($index === false)       return false;   //  No at-sign
        if ($index === 0)           return false;   //  No local part
        if ($index > 64)            return false;   //  Local part too long

        $localPart      = substr($email, 0, $index);
        $domain         = substr($email, $index + 1);
        $domainLength   = strlen($domain);

        if ($domainLength === 0)    return false;   //  No domain part
        if ($domainLength > 255)    return false;   //  Domain part too long

        //  Let's check the local part for RFC compliance...
        //
        //  Period (".") may...appear, but may not be used to start or end the
        //  local part, nor may two or more consecutive periods appear.
        //      (http://tools.ietf.org/html/rfc3696#section-3)
        if (preg_match('/^\\.|\\.\\.|\\.$/', $localPart) > 0)               return false;   //  Dots in wrong place

        //  Any ASCII graphic (printing) character other than the
        //  at-sign ("@"), backslash, double quote, comma, or square brackets may
        //  appear without quoting.  If any of that list of excluded characters
        //  are to appear, they must be quoted
        //      (http://tools.ietf.org/html/rfc3696#section-3)
        if (preg_match('/^"(?:.)*"$/', $localPart) > 0) {
            //  Local part is a quoted string
            if (preg_match('/(?:.)+[^\\\\]"(?:.)+/', $localPart) > 0)   return false;   //  Unescaped quote character inside quoted string
        } else {
            if (preg_match('/[ @\\[\\]\\\\",]/', $localPart) > 0)
                //  Check all excluded characters are escaped
                $stripped = preg_replace('/\\\\[ @\\[\\]\\\\",]/', '', $localPart);
            if (preg_match('/[ @\\[\\]\\\\",]/', $stripped) > 0)        return false;   //  Unquoted excluded characters
        }

        //  Now let's check the domain part...

        //  The domain name can also be replaced by an IP address in square brackets
        //      (http://tools.ietf.org/html/rfc3696#section-3)
        //      (http://tools.ietf.org/html/rfc5321#section-4.1.3)
        //      (http://tools.ietf.org/html/rfc4291#section-2.2)
        if (preg_match('/^\\[(.)+]$/', $domain) === 1) {
            //  It's an address-literal
            $addressLiteral = substr($domain, 1, $domainLength - 2);
            $matchesIP      = array();

            //  Extract IPv4 part from the end of the address-literal (if there is one)
            if (preg_match('/\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/', $addressLiteral, $matchesIP) > 0) {
                $index = strrpos($addressLiteral, $matchesIP[0]);

                if ($index === 0) {
                    //  Nothing there except a valid IPv4 address, so...
                    return true;
                } else {
                    //  Assume it's an attempt at a mixed address (IPv6 + IPv4)
                    if ($addressLiteral[$index - 1] !== ':')            return false;   //  Character preceding IPv4 address must be ':'
                    if (substr($addressLiteral, 0, 5) !== 'IPv6:')      return false;   //  RFC5321 section 4.1.3

                    $IPv6 = substr($addressLiteral, 5, ($index ===7) ? 2 : $index - 6);
                    $groupMax = 6;
                }
            } else {
                //  It must be an attempt at pure IPv6
                if (substr($addressLiteral, 0, 5) !== 'IPv6:')          return false;   //  RFC5321 section 4.1.3
                $IPv6 = substr($addressLiteral, 5);
                $groupMax = 8;
            }

            $groupCount = preg_match_all('/^[0-9a-fA-F]{0,4}|\\:[0-9a-fA-F]{0,4}|(.)/', $IPv6, $matchesIP);
            $index      = strpos($IPv6,'::');

            if ($index === false) {
                //  We need exactly the right number of groups
                if ($groupCount !== $groupMax)                          return false;   //  RFC5321 section 4.1.3
            } else {
                if ($index !== strrpos($IPv6,'::'))                     return false;   //  More than one '::'
                $groupMax = ($index === 0 || $index === (strlen($IPv6) - 2)) ? $groupMax : $groupMax - 1;
                if ($groupCount > $groupMax)                            return false;   //  Too many IPv6 groups in address
            }

            //  Check for unmatched characters
            array_multisort($matchesIP[1], SORT_DESC);
            if ($matchesIP[1][0] !== '')                                    return false;   //  Illegal characters in address

            //  It's a valid IPv6 address, so...
            return true;
        } else {
            //  It's a domain name...

            //  The syntax of a legal Internet host name was specified in RFC-952
            //  One aspect of host name syntax is hereby changed: the
            //  restriction on the first character is relaxed to allow either a
            //  letter or a digit.
            //      (http://tools.ietf.org/html/rfc1123#section-2.1)
            //
            //  NB RFC 1123 updates RFC 1035, but this is not currently apparent from reading RFC 1035.
            //
            //  Most common applications, including email and the Web, will generally not permit...escaped strings
            //      (http://tools.ietf.org/html/rfc3696#section-2)
            //
            //  Characters outside the set of alphabetic characters, digits, and hyphen MUST NOT appear in domain name
            //  labels for SMTP clients or servers
            //      (http://tools.ietf.org/html/rfc5321#section-4.1.2)
            //
            //  RFC5321 precludes the use of a trailing dot in a domain name for SMTP purposes
            //      (http://tools.ietf.org/html/rfc5321#section-4.1.2)
            $matches    = array();
            $groupCount = preg_match_all('/(?:[0-9a-zA-Z][0-9a-zA-Z-]{0,61}[0-9a-zA-Z]|[a-zA-Z])(?:\\.|$)|(.)/', $domain, $matches);
            $level      = count($matches[0]);

            if ($level == 1)                                            return false;   //  Mail host can't be a TLD

            $TLD = $matches[0][$level - 1];
            if (substr($TLD, strlen($TLD) - 1, 1) === '.')              return false;   //  TLD can't end in a dot
            if (preg_match('/^[0-9]+$/', $TLD) > 0)                     return false;   //  TLD can't be all-numeric

            //  Check for unmatched characters
            array_multisort($matches[1], SORT_DESC);
            if ($matches[1][0] !== '')                          return false;   //  Illegal characters in domain, or label longer than 63 characters

            //  Check DNS?
            if ($checkDNS && function_exists('checkdnsrr')) {
                if (!(checkdnsrr($domain, 'A') || checkdnsrr($domain, 'MX'))) {
                    return false;   //  Domain doesn't actually exist
                }
            }

            //  Eliminate all other factors, and the one which remains must be the truth.
            //      (Sherlock Holmes, The Sign of Four)
            return true;
        }
    }

    public static function getIp()
    {
        //取得USER來源IP
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip=$_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    public static function banIp($ip, $allIpAry)
    {
        if (is_array($allIpAry) && !in_array($ip, $allIpAry)) {
            header("Location: http://fet.xmas.hiiir.com");
            exit;
        }
    }

    /**
     * function defination to convert array to SimpleXMLElement Obj
     *
     * 用法： $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><root/>');
     *       $xml = arrayToXmlobj($output, $xml);
     *       print $xml->asXML();  //轉為xml印出
     *
     * function 採 non-defensive programming，參數傳入為array, simpleXMLElement為caller義務
     *
     * @param Array $inputArray
     * @param SimpleXMLElement $simpleXMLElementObj
     * @return 格式化後的$text
     */
    public static function arrayToXmlobj($inputArray, $simpleXMLElementObj)
    {
        foreach($inputArray as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)) {
                    $subnode = $simpleXMLElementObj->addChild("$key");
                    self::arrayToXmlobj($value, $subnode);
                }
                else {
                    self::arrayToXmlobj($value, $simpleXMLElementObj);
                }
            }
            else {
                $simpleXMLElementObj->addChild("$key","$value");
            }
        }
        return $simpleXMLElementObj;
    }

    public static function arrayToXmlStr($inputArray)
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><root/>');
        $xml = self::arrayToXmlobj($inputArray, $xml);
        return $xml->asXML();  //轉為str
    }

    // json_encode 會把 simpleObj 轉為 json
    public static function xmlToAry($xml)
    {
        return json_decode(json_encode((array)simplexml_load_string($xml)),1);
    }

    public static function paramIsEmpty($param)
    {
        if (is_array($param)) {
            foreach ($param as $key => $value) {
                if (empty($value)) {
                    return true;
                }
            }
        } else {
            if (empty($param)) {
                return true;
            }
        }
        return false;
    }

    public static function nullToStr(&$item, $key = 0)
    {
        if (is_null($item)) {
            $item = "";
        }
    }

    public static function excludeNull($rawData)
    {
        if (is_array($rawData)) {
            array_walk_recursive($rawData, "self::nullToStr");
        } else {
            self::nullToStr($rawData);  // or strval()
        }
        return $rawData;
    }

    public static function isParamMissing($param)
    {
        if (is_array($param)) {
            foreach ($param as $key => $value) {
                if ($value === "" || is_null($value)) {
                    return true;
                }
            }
        } else {
            if ($param === "" || is_null($value)) {
                return true;
            }
        }
        return false;
    }


    //刪除資料夾內過期檔案
    public static function deleteExpiredFile($dir, $expiredTime)
    {
        $fileList = opendir($dir);
        while ($file = readdir($fileList)) {
            if ($file != "." && $file != ".." && $file != ".svn") {
                $path = $dir . "/" . $file;
                if (floor(time()-fileatime($path)) > $expiredTime) {
                    unlink($path);
                }
            }
        }
        closedir($fileList);
        unset($file);
    }


    //暫存資料夾確認，刪除過期檔案
    //與config耦合 再看怎麼改
    public static function checkTmpDir()
    {
        if (!is_dir(TMP_UPLOAD_DIR)) {
            $result = mkdir(TMP_UPLOAD_DIR);
        } else {
            self::deleteExpiredFile(TMP_UPLOAD_DIR, TMP_EXPIRED_TIME);
        }
    }

    public static function checkDir($dir)
    {
        if (!is_dir($dir)) {
            mkdir($dir, 0775, true);
        }
    }

    public static function getImageExtension($imagePath)
    {
        list($width, $height, $type, $attr) = getimagesize($imagePath);
        $extensionMap = array( 1=>'gif', 2=>'jpg', 3=>'png');
        return $extensionMap[$type];
    }

    public static function errorResponse($condition, $errorMessage= "error")
    {
        $comparisonTable = array(
                            400                     => 400,
                            404                     => 404,
                            405                     => 405,
                            500                     => 500,
                            "not found"             => 404,
                            "not allowed"           => 405,
                            "internal server error" => 500,
                            );
        return $comparisonTable[$condition] ?
                                                array("result" => $errorMessage, "errcode" => $comparisonTable[$condition] )
                                            :   array("result" => $errorMessage, "errcode" => $condition );
    }

    // 取得圖片順時鐘翻轉角度
    public static function getRotateAngle($imgPath)
    {
        $exif = @exif_read_data($imgPath, 0, false);

        switch ($exif['Orientation']) {
            case 8:
                return 90;
            case 3:
                return 180;
            case 6:
                return 270;
            default:
                return 0;
        }

    }

    // xml convert 沒測過
    public static function outputConvert($output, $outputType)
    {
        //將array的null轉成""
        $output = self::excludeNull($output);

        //將變數output中的array轉換格式並輸出
        switch ($outputType) {
            case "JSON":
                echo json_encode($output);
                break;
            case "ARRAY":
                print_r($output);
                break;
            case "XML":
                //$output = array_flip($output);  //反轉 xml轉換會反轉回來

                /*$xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><root/>');
                $xml = arrayToXml($output, $xml);
                print $xml->asXML();*/

                print self::arrayToXmlStr($output);
                break;
            default:
                echo 'coding wrong type';
                break;
        }
    }


    public static function parseUriToSite()
    {
        $uriParse = explode("/", $_SERVER["REQUEST_URI"]);
        return array("site"=> $uriParse[1], "page"=> $uriParse[2]);
    }

    public static function getPath()
    {
        $path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
        $path = $path == "/" ? "index" : $path;
        return $path;
    }

    public static function getFbName($fbId)
    {
        $response = file_get_contents("https://graph.facebook.com/" . $fbId . "?fields=name");
        $user = json_decode($response,true);
        return $user["name"];
    }

    public static function getFbPicture($fbId)
    {
        $response = file_get_contents("https://graph.facebook.com/" . $fbId . "?fields=picture.type(large)");
        $user = json_decode($response,true);
        return $user["picture"]["data"]["url"];
    }

    // generate version 4 random guid
    public static function createGuid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        } else {
            return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
                mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        }
    }

    // for phalcon
    // $paramAry 格式：
    // array( "column" => $value,
    //        "column" => $value,
    // );
    //
    // TODO: 移到phalcon class extend model
    public static function buildModelFindConditionParams($paramAry)
    {
        $conditionsAry = array();
        $bindAry = array();

        foreach ($paramAry as $column => $value) {
            $conditionsAry[] = $column . " = :" . $column . ":";
            $bindAry[$column] = $value;
        }

        $conditionsStr = implode(" AND ", $conditionsAry);
        return array("conditions" => $conditionsStr, "bind" => $bindAry);
    }

    // 重新命名
    // for phalcon
    // $paramAry 格式：
    // array( "column" => $value,
    //        "column" => $value,
    // );
    //
    // TODO: 移到phalcon class extend model
    public static function getFindAry($paramAry)
    {
        if (! is_array($paramAry)) {
            return array();
        }

        $conditionsAry = array();
        $bindAry = array();

        foreach ($paramAry as $column => $value) {
            $conditionsAry[] = $column . " = :" . $column . ":";
            $bindAry[$column] = $value;
        }

        $conditionsStr = implode(" AND ", $conditionsAry);
        return array("conditions" => $conditionsStr, "bind" => $bindAry);
    }

}

