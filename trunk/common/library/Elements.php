<?php
/**
 * Class and Function List:
 * Function list:
 * - getMenu()
 * - generateMenu()
 * - getPageHeader()
 * Classes list:
 * - Elements extends Phalcon
 */

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Phalcon\Mvc\User\Component
{
    private $_menu = array(
        "stack" => array(
            array(
                "name" => "最新消息",
                "href" => "news",
                "icon" => "icon-edit"
            ) ,
            array(
                "name" => "app上架",
                "href" => "product",
                "icon" => "icon-mobile-phone"
            ) ,
            array(
                "name" => "廣告上架",
                "href" => "adds",
                "icon" => "icon-mobile-phone"
            ) ,
        )
    );
    
    private $_pageHeader = array();
    
    // nav 結構：
    // <nav id='main-nav'>
    //   <div class='navigation'>
    //     <ul class='nav nav-stacked'>
    //       <li>
    //         <a class='dropdown-collapse' href='#'>
    //           <i class='icon-edit'></i>
    //           <span>基本資料</span>
    //           <i class='icon-angle-down angle-down'></i>
    //         </a>
    //         <ul class='nav nav-stacked'>
    //           <li>
    //             <a class='dropdown-collapse' href='{{ url('product/list')}}'>
    //               <i class='icon-caret-right'></i>
    //               <span>商品</span>
    //               <i class='icon-angle-down angle-down'></i>
    //             </a>
    //           </li>
    //         </ul>
    //       </li>
    //     </ul>
    //   </div>
    // </nav>
    
    // 結構 簡化：
    // a
    // ul
    //   li
    //     repeat
    //    /li
    // /ul
    
    public function getMenu()
    {
        $this->generateMenu($this->_menu);
    }
    
    //  a 結構
    //  <a class='dropdown-collapse' href='#'>
    //    <i class='icon-edit'></i>
    //    <span>基本資料</span>
    //    <i class='icon-angle-down angle-down'></i>
    //  </a>
    private function generateMenu($menu)
    {
        $dispatcher = $this->di->getShared('dispatcher');
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        
        $in = (is_array($menu["in"][$controller]) && in_array($action, $menu["in"][$controller])) ? "in" : "";
        
        if (isset($menu["name"])) {
            $url = $this->di->getShared('url');
            
            $href = $menu["href"] ? $url->get($menu["href"]) : "#";
            $aClass = isset($menu["stack"]) ? "dropdown-collapse" : "";
            $icon = isset($menu["icon"]) ? $menu["icon"] : "icon-caret-right";
            $name = $menu["name"];
            $iconDropdown = isset($menu["stack"]) ? "<i class='icon-angle-down angle-down'></i>" : "";
            
            echo "<a href='" . $href . "' class='" . $aClass . " " . $in . "'>";
            echo "<i class='" . $icon . "'></i>";
            echo "<span>" . $name . "</span>";
            echo $iconDropdown;
            echo "</a>";
        }
        
        if (isset($menu["stack"])) {
            echo "<ul class='nav nav-stacked " . $in . "'>";
            foreach ($menu["stack"] as $submenu) {
                if (is_array($submenu["active"][$controller]) && in_array($action, $submenu["active"][$controller])) {
                    echo "<li class='active'>";
                } else {
                    echo "<li>";
                }
                $this->generateMenu($submenu);
                echo "</li>";
            }
            echo "</ul>";
        }
    }
    
    public function getPageHeader()
    {
    }
}
