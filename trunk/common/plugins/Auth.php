<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - getAcl()
 * - beforeDispatch()
 * Classes list:
 * - Auth extends Plugin
 */
use Phalcon\Events\Event, Phalcon\Mvc\User\Plugin, Phalcon\Mvc\Dispatcher, Phalcon\Acl;
/**
 * Auth
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Auth extends Plugin
{
    
    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }
    // resource 意同 controller
    // resource 結構：
    //  $publicResources = array(
    //      'index' => array('index'),
    //      'about' => array('index'),
    //      'session' => array('index', 'register', 'start', 'end'),
    //      'contact' => array('index', 'send')
    //  );
    //
    public function getAcl($role, $adminId)
    {
        // 無 acl , new
        if (isset($this->persistent->acl) && ACL_CACHE_OPEN) {
            $acl = $this->persistent->acl;
        } else {
            $acl = new Phalcon\Acl\Adapter\Memory();
            $acl->setDefaultAction(Phalcon\Acl::DENY);
        }
        // 無此 role ，計算 acl
        if (!$acl->isRole($role)) {
            $acl->addRole(new Phalcon\Acl\Role($role));
            // 取公開與 admin_id
            $adminAuthDataObj = Qbon\Models\AdminAuth::find(array(
                "conditions" => "admin_id = :admin_id: OR admin_id = 0",
                "bind" => array(
                    "admin_id" => $adminId
                )
            ));
            
            foreach ($adminAuthDataObj as $key => $adminAuth) {
                $resourceAry[$adminAuth->controller][] = $adminAuth->action;
            }
            
            foreach ($resourceAry as $resource => $actionAry) {
                $acl->addResource(new Phalcon\Acl\Resource($resource) , $actionAry);
            }
            
            foreach ($resourceAry as $resource => $actionAry) {
                $acl->allow($role, $resource, $actionAry);
            }
            $this->persistent->acl = $acl;
        }
        
        return $this->persistent->acl;
    }
    /**
     * This action is executed before execute any action in the application
     * 設定角色 -> 取得 acl -> is allow
     *
     * 1. 登出或第一次進入, 刪除 session 中 acl, 並設為訪客
     * 2. 取得 acl ，session 中若無 acl 會計算 resource
     * 3. is allow
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        if (!$this->session->has("auth")) {
            $this->persistent->destroy();
            $this->session->set('auth', array(
                'admin_id' => 0, // adminId == 0 為公開權限
                'account' => "Guests"
            ));
        }
        
        $auth = $this->session->get('auth');
        
        $role = $auth["account"];
        $adminId = $auth["admin_id"];
        
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
        
        if ($role == "Guests" and $action != "signin") {
            $dispatcher->forward(array(
                "controller" => "index",
                "action" => "signin"
            ));
        }
        
        $acl = $this->getAcl($role, $adminId);
        // var_dump($role); var_dump($controller); var_dump($action); var_dump($acl->isAllowed($role, $controller, $action)); exit;
        // $allowed = $acl->isAllowed($role, $controller, $action);
        $allowed = Acl::ALLOW;
        
        if ($allowed != Acl::ALLOW) {
            if ($controller != "index" || $action != "index") {
                $this->flash->error("You don't have access to this module");
            }
            if ($role == "Guests") {
                $dispatcher->forward(array(
                    "controller" => "index",
                    "action" => "signin"
                ));
            } else {
                $dispatcher->forward(array(
                    "controller" => "index",
                    "action" => "index"
                ));
            }
            return false;
        }
    }
}
