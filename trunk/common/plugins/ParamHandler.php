<?php
/**
 * Class and Function List:
 * Function list:
 * - __construct()
 * - beforeDispatchLoop()
 * - isSequential()
 * Classes list:
 * - ParamHandler extends Plugin
 */
use Phalcon\Events\Event, Phalcon\Mvc\User\Plugin, Phalcon\Mvc\Dispatcher;

/**
 * ParamHandler
 *
 * 將 params 轉為 key => value 格式
 */
class ParamHandler extends Plugin
{
    
    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }
    
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $params = $dispatcher->getParams();
        if ($this->isSequential($params)) {
            $keyParams = array();
            foreach ($params as $number => $value) {
                if ($number & 1) {
                    
                    // 等於 %2
                    $keyParams[$params[$number - 1]] = $value;
                }
            }
            $dispatcher->setParams($keyParams);
        }
    }
    
    // 確認 array 是關連或循序
    private function isSequential($array)
    {
        return (bool)count(array_filter(array_keys($array) , 'is_int'));
    }
}
