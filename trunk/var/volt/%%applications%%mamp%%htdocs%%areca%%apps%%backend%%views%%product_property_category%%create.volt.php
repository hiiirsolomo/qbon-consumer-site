<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <div class='title'>新增</div>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('product_property_category/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;')); ?>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product_property_category_id'>流水號</label>
            <div class='col-sm-4 controls'>

            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product_property_category_name'>分類名</label>
            <div class='col-sm-4 controls'>
              <input value='' class='form-control' data-rule-required='true' id='product_property_category_name' name='name' placeholder='分類名' type='text'>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='flag-active'>上/下架</label>
            <div class='col-sm-4 controls'>
              <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success">
                <input checked='checked' type='checkbox' name="flag_active" id="flag-active">
              </div>
            </div>
          </div>

          <div class='form-actions' style='margin-bottom:0'>
            <div class='row'>
              <div class='col-sm-9 col-sm-offset-3'>
                <button class='btn btn-success' type='submit'>
                  <i class='icon-save'></i>
                  儲存
                </button>
              </div>
            </div>
          </div>
        </form>
        <?php echo $end_form; ?>
      </div>
    </div>
  </div>
</div>