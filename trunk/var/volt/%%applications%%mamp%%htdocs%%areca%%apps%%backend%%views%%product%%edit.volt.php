<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <div class='title'>編輯</div>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('product/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;', 'enctype' => 'multipart/form-data')); ?>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product_id'>流水號</label>
            <div class='col-sm-4 controls'>
              <?php echo $productObj->product_id; ?>
              <input value='<?php echo $productObj->product_id; ?>' class='form-control' data-rule-required='true' id='product_id' name='product_id' placeholder='分類名' type='hidden'>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product_difference_category_name'>分類</label>
            <div class='col-sm-4 controls'>
              <select class='select2 form-control' name="product_difference_category_id">
                  <?php foreach ($productPropertyCategoryRowSetObj as $productPropertyCategoryObj) { ?>
                    <optgroup label='<?php echo $productPropertyCategoryObj->name; ?>'>
                    <?php foreach ($productDifferenceCategoryRowSetObj as $productDifferenceCategoryObj) { ?>
                        <?php if ($productPropertyCategoryObj->product_property_category_id == $productDifferenceCategoryObj->product_property_category_id) { ?>
                          <?php if ($productObj->product_difference_category_id == $productDifferenceCategoryObj->product_difference_category_id) { ?>
                          <option value='<?php echo $productDifferenceCategoryObj->product_difference_category_id; ?>' selected><?php echo $productDifferenceCategoryObj->name; ?></option>
                          <?php } else { ?>
                          <option value='<?php echo $productDifferenceCategoryObj->product_difference_category_id; ?>'><?php echo $productDifferenceCategoryObj->name; ?></option>
                          <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    </optgroup>
                  <?php } ?>
              </select>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product-name'>名稱</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='name' placeholder='分類名' type='text'>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='product-img'>產品圖</label>
            <div class='col-sm-4 controls'>
              <?php if ($productObj->img) { ?>
              <?php echo $this->tag->image(array($productObj->img, 'width' => '100')); ?>
              <?php } ?>
              <input title='選擇產品圖' type='file' name="img">
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='flag-active'>上/下架</label>
            <div class='col-sm-4 controls'>
              <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success">
                <?php if ($productObj->flag_active == 'N') { ?>
                <input type='checkbox' name="flag_active" id="flag-active">
                <?php } else { ?>
                <input type='checkbox' name="flag_active" id="flag-active" checked='checked'>
                <?php } ?>
              </div>
            </div>
          </div>

          <div class='form-actions' style='margin-bottom:0'>
            <div class='row'>
              <div class='col-sm-9 col-sm-offset-3'>
                <button class='btn btn-success' type='submit'>
                  <i class='icon-save'></i>
                  儲存
                </button>
              </div>
            </div>
          </div>
        </form>
        <?php echo $end_form; ?>
      </div>
    </div>
  </div>
</div>