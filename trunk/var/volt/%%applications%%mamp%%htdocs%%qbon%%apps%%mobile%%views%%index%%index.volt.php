<div id="content">
    <div id="grid-lists">
    <?php if ($appsObj) { ?>
    <?php foreach ($appsObj as $appsRow) { ?>
        <div class="gird">
            <div class="gird-img">
                <img src="<?php echo $this->url->get($appsRow->img); ?>" width="70" height="70" alt="">
            </div>
            <div class="gird-data">
            <h3><a href=""><?php echo $appsRow->name; ?></a></h3>
                <p><?php echo nl2br($appsRow->desc); ?></p>
            </div>
            <a href="<?php echo $appsRow->apple_link; ?>" class="btn-pink download"></a>
        </div>
    <?php } ?>
    <?php } ?>

        <a href="" class="btn-dark">載入更多</a>
    </div>
</div>
