<div class="box">
  <div class="box-header blue-background">
    <div class="title">
      <div class="icon-edit"></div>
      最新消息表格
    </div>
  </div>
  <?php if ($newsObj) { ?>
  <?php $title = $newsObj->title; ?>
  <?php $content = $newsObj->content; ?>
  <?php $hidden = $this->tag->hiddenField(array('news_id', 'value' => $newsObj->news_id)); ?>
  <?php } ?>

  <div class="box-content">
    <form class="form form-horizontal" style="margin-bottom: 0;" method="post" action="<?php echo $this->url->get('news/update'); ?>" accept-charset="UTF-8">
      <div class="form-group">
        <label class="col-md-2 control-label" for="inputText1">標題</label>
        <div class="col-md-8">
          <?php echo $hidden; ?>
          <input class="form-control" id="inputText1" name="title" placeholder="標題" type="text" value="<?php echo $title; ?>">
        </div>
      </div>

      <div class="form-group">
       <label class="col-md-2 control-label" for="inputText1">選擇日期</label>
       <div class="col-md-3">                   

        <div class="datepicker input-group" id="datepicker">
          <input class="form-control" name="publish_time" value="<?php echo $newsObj->publish_time; ?>" data-format="yyyy-MM-dd" placeholder="Select datepicker" type="text">
          <span class="input-group-addon">
            <span data-date-icon="icon-calendar" data-time-icon="icon-time" class="icon-calendar"></span>
          </span>
        </div>
      </div>
    </div>


    <div class="form-group">
      <label class="col-md-2 control-label" for="inputTextArea1">內容</label>

      <div class="col-md-8">
        <textarea class='form-control ckeditor' id='wysiwyg1' rows='20' name='content'><?php echo $content; ?></textarea>
      </div>
    </div>




    <div class="form-actions form-actions-padding-sm">
      <div class="row">
        <div class="col-md-10 col-md-offset-2">
          <button class="btn btn-primary" type="submit">
            <i class="icon-save"></i>
            Save
          </button>
        </div>
      </div>
    </div>
  </form>

</div>
</div>