<?php echo $this->getContent(); ?>

<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='col-xs-12 col-md-1'>
          <?php echo $this->tag->linkTo(array('origin/create', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn')); ?>
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    名稱
                  </th>
                  <th>
                    聯絡人
                  </th>
                  <th>
                    電話
                  </th>
                  <th>
                    地址
                  </th>
                  <th>
                    編輯
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($originRowSetObj as $originObj) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($originObj->origin_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($originObj->name); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($originObj->contact); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($originObj->phone); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($originObj->address); ?></td>
                  <td>
                    <div class='text-right'>
                      <?php echo $this->tag->linkTo(array('origin/edit/' . $originObj->origin_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('origin/delete/' . $originObj->origin_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>