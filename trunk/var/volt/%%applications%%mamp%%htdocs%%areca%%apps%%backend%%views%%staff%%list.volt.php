<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='title'>列表</div>
        <div class='pull-right'>
          <?php echo $this->tag->linkTo(array('staff/edit', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn', 'style' => 'margin-bottom:5px')); ?>
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    員工名稱
                  </th>
                  <th>
                    所屬店別
                  </th>
                  <th>
                    電話
                  </th>
                  <th>
                    地址
                  </th>
                  <th>
                    備註
                  </th>
                  <th>
                    Status
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($staffRowSetObj as $staffObj) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($staffObj->staff_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($staffObj->name); ?></td>
                  <td>
                  <?php foreach ($storeRowSetObj as $storeObj) { ?>
                    <?php if ($storeObj->store_id == $staffObj->store_id) { ?>
                    <?php echo $this->escaper->escapeHtml($storeObj->name); ?>
                    <?php } ?>
                  <?php } ?>
                  </td>
                  <td><?php echo $this->escaper->escapeHtml($staffObj->phone); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($staffObj->address); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($staffObj->comment); ?></td>
                  <td>
                    <?php if ($staffObj->flag_active == 'Y') { ?>
                    <span class='label label-success'>上架</span>
                    <?php } else { ?>
                    <span class='label label-warning'>下架</span>
                    <?php } ?>
                  </td>
                  <td>
                    <div class='text-right'>
                      <?php echo $this->tag->linkTo(array('staff/edit/' . $staffObj->staff_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('staff/delete/' . $staffObj->staff_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>