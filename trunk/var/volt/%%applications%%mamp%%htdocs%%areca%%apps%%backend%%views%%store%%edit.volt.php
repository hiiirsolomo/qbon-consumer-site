<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <?php if (isset($storeObj->store_id)) { ?>
        <div class='title'>編輯</div>
        <?php } else { ?>
        <div class='title'>新增</div>
        <?php } ?>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('store/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;')); ?>
          <?php if (isset($storeObj->store_id)) { ?>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='store_id'>流水號</label>
            <div class='col-sm-4 controls'>
              <?php echo $storeObj->store_id; ?>
              <input value='<?php echo $storeObj->store_id; ?>' class='form-control' data-rule-required='true' id='store_id' name='store_id' placeholder='分類名' type='hidden'>
            </div>
          </div>
          <?php } ?>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='store_name'>店鋪名稱</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $storeObj->name; ?>' class='form-control' data-rule-required='true' id='store_name' name='name' placeholder='店鋪名稱' type='text'>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='type'>類型</label>
            <div class='col-sm-4 controls'>
              <select class='form-control' name="type" id="type">
                  <?php foreach ($storeTypeAry as $storeType => $storeTypeName) { ?>
                    <?php if ($storeObj->type == $storeType) { ?>
                      <option value='<?php echo $storeType; ?>' selected><?php echo $storeTypeName; ?></option>
                    <?php } else { ?>
                      <option value='<?php echo $storeType; ?>'><?php echo $storeTypeName; ?></option>
                    <?php } ?>
                  <?php } ?>
              </select>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='phone'>電話</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $storeObj->phone; ?>' class='form-control' data-rule-required='true' id='phone' name='phone' placeholder='電話' type='text'>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='contact'>聯絡人</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $storeObj->contact; ?>' class='form-control' data-rule-required='true' id='contact' name='contact' placeholder='聯絡人' type='text'>
            </div>
          </div>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='cellphone'>地區</label>
            <div class='col-sm-4 controls'>
              <div id="twzipcode" class="form-inline">
                  <div data-role="county" data-style="form-control" data-value="<?php echo $storeObj->county; ?>"></div>
                  <div data-role="district" data-style="form-control" data-value="<?php echo $storeObj->district; ?>"></div>
                  <div data-role="zipcode" data-style="form-control" data-value="<?php echo $storeObj->zipcode; ?>"></div>
                </div>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='address'>地址</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $storeObj->address; ?>' class='form-control' data-rule-required='true' id='address' name='address' placeholder='地址' type='text'>
            </div>
          </div>


          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='flag-active'>上/下架</label>
            <div class='col-sm-4 controls'>
              <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success">
                <?php if ($storeObj->flag_active == 'N') { ?>
                <input type='checkbox' name="flag_active" id="flag-active">
                <?php } else { ?>
                <input checked='checked' type='checkbox' name="flag_active" id="flag-active">
                <?php } ?>
              </div>
            </div>
          </div>

          <div class='form-actions' style='margin-bottom:0'>
            <div class='row'>
              <div class='col-sm-9 col-sm-offset-3'>
                <button class='btn btn-success' type='submit'>
                  <i class='icon-save'></i>
                  儲存
                </button>
              </div>
            </div>
          </div>
        </form>
        <?php echo $end_form; ?>
      </div>
    </div>
  </div>
</div>