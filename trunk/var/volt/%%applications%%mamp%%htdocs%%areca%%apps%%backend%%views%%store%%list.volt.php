<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='title'>列表</div>
        <div class='pull-right'>
          <?php echo $this->tag->linkTo(array('store/edit', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn', 'style' => 'margin-bottom:5px')); ?>
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    店鋪名稱
                  </th>
                  <th>
                    類型
                  </th>
                  <th>
                    電話
                  </th>
                  <th>
                    聯絡人
                  </th>
                  <th>
                    縣/市
                  </th>
                  <th>
                    區
                  </th>
                  <th>
                    郵遞區號
                  </th>
                  <th>
                    地址
                  </th>
                  <th>
                    Status
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($storeRowSetObj as $storeObj) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->store_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->name); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeTypeAry[$storeObj->type]); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->phone); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->contact); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->county); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->district); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->zipcode); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($storeObj->address); ?></td>
                  <td>
                    <?php if ($storeObj->flag_active == 'Y') { ?>
                    <span class='label label-success'>上架</span>
                    <?php } else { ?>
                    <span class='label label-warning'>下架</span>
                    <?php } ?>
                  </td>
                  <td>
                    <div class='text-right'>
                      <?php echo $this->tag->linkTo(array('store/edit/' . $storeObj->store_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('store/delete/' . $storeObj->store_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>