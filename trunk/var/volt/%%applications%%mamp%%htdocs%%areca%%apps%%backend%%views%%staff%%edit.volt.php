<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <?php if (isset($staffObj->staff_id)) { ?>
        <div class='title'>編輯</div>
        <?php } else { ?>
        <div class='title'>新增</div>
        <?php } ?>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('staff/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;')); ?>
          <?php if (isset($staffObj->staff_id)) { ?>
          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='staff_id'>流水號</label>
            <div class='col-sm-4 controls'>
              <?php echo $staffObj->staff_id; ?>
              <input value='<?php echo $staffObj->staff_id; ?>' class='form-control' data-rule-required='true' id='staff_id' name='staff_id' placeholder='分類名' type='hidden'>
            </div>
          </div>
          <?php } ?>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='staff_name'>員工名稱</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $staffObj->name; ?>' class='form-control' data-rule-required='true' id='staff_name' name='name' placeholder='員工名稱' type='text'>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='store_id'>所屬店別</label>
            <div class='col-sm-4 controls'>
              <select class='select2 form-control' name="store_id" id="store_id">
                  <option value='' selected>不選擇</option>
                  <?php foreach ($storeRowSetObj as $storeObj) { ?>
                    <?php if ($storeObj->store_id == $staffObj->store_id) { ?>
                      <option value='<?php echo $storeObj->store_id; ?>' selected><?php echo $storeObj->name; ?></option>
                    <?php } else { ?>
                      <option value='<?php echo $storeObj->store_id; ?>'><?php echo $storeObj->name; ?></option>
                    <?php } ?>
                  <?php } ?>
              </select>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='phone'>電話</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $staffObj->phone; ?>' class='form-control' data-rule-required='true' id='phone' name='phone' placeholder='電話' type='text'>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='address'>地址</label>
            <div class='col-sm-4 controls'>
              <input value='<?php echo $staffObj->address; ?>' class='form-control' data-rule-required='true' id='address' name='address' placeholder='地址' type='text'>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='comment'>備註</label>
            <div class='col-sm-4 controls'>
              <textarea class='form-control' id='comment' placeholder='備註' rows='3' name="comment"><?php echo $staffObj->comment; ?></textarea>
            </div>
          </div>

          <div class='form-group'>
            <label class='control-label col-sm-3 col-sm-3' for='flag-active'>上/下架</label>
            <div class='col-sm-4 controls'>
              <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success">
                <?php if ($staffObj->flag_active == 'N') { ?>
                <input type='checkbox' name="flag_active" id="flag-active">
                <?php } else { ?>
                <input checked='checked' type='checkbox' name="flag_active" id="flag-active">
                <?php } ?>
              </div>
            </div>
          </div>

          <div class='form-actions' style='margin-bottom:0'>
            <div class='row'>
              <div class='col-sm-9 col-sm-offset-3'>
                <button class='btn btn-success' type='submit'>
                  <i class='icon-save'></i>
                  儲存
                </button>
              </div>
            </div>
          </div>
        </form>
        <?php echo $end_form; ?>
      </div>
    </div>
  </div>
</div>