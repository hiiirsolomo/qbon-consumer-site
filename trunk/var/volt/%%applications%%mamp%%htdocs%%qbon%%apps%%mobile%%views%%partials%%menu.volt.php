<?php if ($menuType == 'origin') { ?>

<div id="nav-content">
    <ul>
        <li class="tab-1 <?php echo $menu['index']; ?>"><a href="/mobile/index#nav-content">聯盟Apps</a></li>
        <li class="tab-2 <?php echo $menu['how']; ?>"><a href="/mobile/how">如何取得</a></li>
        <li class="tab-3 <?php echo $menu['redeem']; ?>"><a href="/mobile/redeem">如何兌換</a></li>
    </ul>
</div>

<?php } elseif ($menuType == 'member') { ?>

<div id="nav-content">
    <ul>
        <li class="tab-1 <?php echo $menu['profile']; ?>"><a href="/mobile/profile">個人資訊</a></li>
        <li class="tab-2 <?php echo $menu['wallet']; ?>"><a href="/mobile/wallet">我的口袋</a></li>
        <li class="tab-3 <?php echo $menu['password']; ?>"><a href="/mobile/password">修改密碼</a></li>
    </ul>
</div>

<?php } else { ?>

<?php } ?>