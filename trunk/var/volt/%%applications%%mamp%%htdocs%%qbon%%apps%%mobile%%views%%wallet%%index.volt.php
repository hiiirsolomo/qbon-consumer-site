<div id="nav-sub-content">
    <div class="tab-1 <?php echo $redeemed0; ?>">
        <a href="/mobile/wallet/index/redeemed/0">未兌換</a>
    </div>
    <div class="tab-2 <?php echo $redeemed1; ?>">
        <a href="/mobile/wallet/index/redeemed/1">已兌換</a>
    </div>
</div>

<div id="content">

    <div id="grid-lists" class="mywallet">
        <?php if ($walletObj) { ?>
        <?php foreach ($walletObj as $walletRow) { ?>
        <div class="gird">
            <div class="gird-img">
                <img src="<?php echo $walletRow->image; ?>" width="115" height="70" alt="">
            </div>
            <div class="gird-data">
                <h3><a href=""><?php echo $walletRow->offerName; ?></a></h3>
                <p><?php echo $walletRow->detail; ?></p>
                <div class="footer">
                    <?php if ($walletRow->offerType == 'COUPON') { ?>
                    <img src="/mobilepublic/images/icon_coupon.png" alt="">
                    優惠券
                    <?php } elseif ($walletRow->offerType == 'MISSION') { ?>
                    <img src="/mobilepublic/images/icon_mission.png" alt="">
                    任務
                    <?php } elseif ($walletRow->offerType == 'VOUCHER') { ?>
                    <img src="/mobilepublic/images/icon_voucher.png" alt="">
                    商品劵
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } else { ?>
         目前無資料
        <?php } ?>

    </div>
</div>