<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <div class='title'>新增產地</div>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('origin/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;', 'enctype' => 'multipart/form-data')); ?>

        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-name'>名稱</label>
          <div class='col-sm-4 controls'>
            <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='name' placeholder='名稱' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-name'>聯絡人</label>
          <div class='col-sm-4 controls'>
            <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='contact' placeholder='聯絡人' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-name'>電話</label>
          <div class='col-sm-4 controls'>
            <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='phone' placeholder='電話' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-name'>地址</label>
          <div class='col-sm-4 controls'>
            <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='address' placeholder='地址' type='text'>
          </div>
        </div>

        <div class='form-actions' style='margin-bottom:0'>
          <div class='row'>
            <div class='col-sm-9 col-sm-offset-3'>
              <button class='btn btn-success' type='submit'>
                <i class='icon-save'></i>
                儲存
              </button>
            </div>
          </div>
        </div>
      </form>
      <?php echo $end_form; ?>
    </div>
  </div>
</div>
</div>