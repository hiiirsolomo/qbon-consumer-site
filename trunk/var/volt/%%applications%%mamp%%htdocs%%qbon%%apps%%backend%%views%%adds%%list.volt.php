<?php echo $this->getContent(); ?>

<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
         <div class='title'>apps列表</div>
        <div class='pull-right'>
          <?php echo $this->tag->linkTo(array('adds/create', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn', 'style' => 'margin-bottom:5px')); ?>
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    連結
                  </th>
                  <th>
                    圖片
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($addsRowSetObj as $addsObj) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($addsObj->adds_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($addsObj->link); ?></td>
                  <td><?php echo $this->tag->image(array(stripslashes($addsObj->img), 'width' => '100')); ?></td>
                  <td>
                    <div class='text-right'>
                      <?php echo $this->tag->linkTo(array('adds/edit/adds_id/' . $addsObj->adds_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('adds/delete/adds_id/' . $addsObj->adds_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>