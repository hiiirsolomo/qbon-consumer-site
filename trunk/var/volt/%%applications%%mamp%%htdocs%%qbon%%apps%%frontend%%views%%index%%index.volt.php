
<div id="top-tags">

	<div id="tags-links">
		<ul id="tags-header-navigation" class="tags">
			<li class="tags-item">
				Random Tags:
			</li>
			<?php foreach ($tags as $tagItem) { ?>
			<li class="tags-item">
				<?php echo $this->tag->linkTo(array('tag/' . $tagItem->name, $tagItem->name)); ?>
			</li>
			<?php } ?>
		</ul>
	</div>

</div>

<div class="section-header">
	<h2>Random Albums</h2>
</div>

<?php echo $this->partial('partials/album-list'); ?>
