<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <div class='title'>編輯</div>
      </div>
      <div class='box-content'>


        <?php echo $this->tag->form(array('product/update', 'method' => 'post', 'class' => 'form form-horizontal validate-form', 'style' => 'margin-bottom: 0;', 'enctype' => 'multipart/form-data')); ?>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product_id'>流水號</label>
          <div class='col-sm-4 controls'>
            <?php echo $productObj->product_id; ?>
            <input value='<?php echo $productObj->product_id; ?>' class='form-control' data-rule-required='true' id='product_id' name='product_id' placeholder='分類名' type='hidden'>
          </div>
        </div>

        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-name'>名稱</label>
          <div class='col-sm-4 controls'>
            <input value='<?php echo $productObj->name; ?>' class='form-control' data-rule-required='true' id='product-name' name='name' placeholder='分類名' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-apple-link'>Apple下載位址</label>
          <div class='col-sm-6 controls'>
            <input value='<?php echo $productObj->apple_link; ?>' class='form-control' data-rule-required='true' id='product-apple-link' name='apple_link' placeholder='分類名' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-android-link'>Android下載位址</label>
          <div class='col-sm-6 controls'>
            <input value='<?php echo $productObj->android_link; ?>' class='form-control' data-rule-required='true' id='product-android-link' name='android_link' placeholder='分類名' type='text'>
          </div>
        </div>
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='inputTextArea1'>描述</label>
          <div class='col-md-5'>
            <textarea class='form-control' id='inputTextArea1' placeholder='Textarea' rows='5' name='desc'> <?php echo $productObj->desc; ?> </textarea>
          </div>
        </div>
        
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='product-img'>產品圖</label>
          <div class='col-sm-4 controls'>
            <?php if ($productObj->img) { ?>
            <?php echo $this->tag->image(array($productObj->img, 'width' => '100')); ?>
            <?php } ?>
            <input title='選擇產品圖' type='file' name="img">
          </div>
        </div>

        <div class='form-actions' style='margin-bottom:0'>
          <div class='row'>
            <div class='col-sm-9 col-sm-offset-3'>
              <button class='btn btn-success' type='submit'>
                <i class='icon-save'></i>
                儲存
              </button>
            </div>
          </div>
        </div>
      </form>
      <?php echo $end_form; ?>
    </div>
  </div>
</div>
</div>