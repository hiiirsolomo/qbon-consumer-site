<div class="box">
  <div class="box-header blue-background">
    <div class="title">
      <div class="icon-edit"></div>
      最新消息表格
    </div>
  </div>
  <div class="box-content">
    <form class="form form-horizontal" style="margin-bottom: 0;" method="post" action="update" accept-charset="UTF-8">

      <input name="" type="hidden">
      <div class="form-group">
        <label class="col-md-2 control-label" for="inputText1">標題</label>
        <div class="col-md-8">
          <input class="form-control" id="inputText1" name="title" placeholder="標題" type="text">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="inputTextArea1">Textarea</label>
        <div class="col-md-8">
          <textarea class="form-control" id="inputTextArea1" name="content" placeholder="Textarea" rows="15"></textarea>
        </div>
      </div>
      
      <div class="form-actions form-actions-padding-sm">
        <div class="row">
          <div class="col-md-10 col-md-offset-2">
            <button class="btn btn-primary" type="submit">
              <i class="icon-save"></i>
              Save
            </button>
            <button class="btn" type="submit">Cancel</button>
          </div>
        </div>
      </div>
    </form>

  </div>
</div>