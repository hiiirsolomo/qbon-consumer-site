<?php echo $this->getContent(); ?>

<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='col-xs-12 col-md-1'>
          <?php echo $this->tag->linkTo(array('product/create', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn')); ?>
        </div>

        <?php echo $this->tag->form(array('method' => 'get', 'id' => 'list-form')); ?>
          <div class="col-xs-12 col-md-2 col-md-offset-4">
            <select class='select2 form-control' name="product_property_category_id">
                <option value=''>請選擇&emsp;</option>

                <?php foreach ($productPropertyCategoryAry as $indexProductPropertyCategoryId => $name) { ?>
                  <?php if ($indexProductPropertyCategoryId == $productPropertyCategoryId) { ?>
                    <option value='<?php echo $indexProductPropertyCategoryId; ?>' selected><?php echo $name; ?></option>
                  <?php } else { ?>
                    <option value='<?php echo $indexProductPropertyCategoryId; ?>'><?php echo $name; ?></option>
                  <?php } ?>
                <?php } ?>
            </select>
          </div>
          <div class="col-xs-12 col-md-2">
            <select class='select2 form-control' name="product_difference_category_id">
                <option value=''>請選擇&emsp;</option>
                <?php foreach ($productDifferenceCategoryAry as $indexProductDifferenceCategoryId => $name) { ?>
                  <?php if ($indexProductDifferenceCategoryId == $productDifferenceCategoryId) { ?>
                    <option value='<?php echo $indexProductDifferenceCategoryId; ?>' selected><?php echo $name; ?></option>
                  <?php } else { ?>
                    <option value='<?php echo $indexProductDifferenceCategoryId; ?>'><?php echo $name; ?></option>
                  <?php } ?>
                <?php } ?>
            </select>
          </div>
          <div class="col-xs-12 col-md-2">
            <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success" data-animated="false">
            <!-- <div class='make-switch switch' data-off-label='&lt;i class="icon-remove"&gt;&lt;/i&gt;' data-on-label='&lt;i class="icon-ok"&gt;&lt;/i&gt;' data-on="success"> -->
              <?php if ($flagActive == 'N') { ?>
              <input type='checkbox' name="flag_active" id="flag-active" value="Y">
              <?php } else { ?>
              <input type='checkbox' name="flag_active" id="flag-active" checked='checked' value="Y">
              <?php } ?>
            </div>
          </div>
          <div class="col-xs-12 col-md-1">
          <a href="<?php echo $this->url->get('product/list'); ?>" class='btn' style='margin-bottom:5px'>
            <i class=' icon-refresh'></i>
            列表
          </a>
          </div>
        <?php echo $end_form; ?>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    大分類
                  </th>
                  <th>
                    小分類
                  </th>
                  <th>
                    名稱
                  </th>
                  <th>
                    圖片
                  </th>
                  <th>
                    Status
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($productRowSetObj as $productObj) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($productObj->product_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($productPropertyCategoryAry[$productObj->product_property_category_id]); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($productDifferenceCategoryAry[$productObj->product_difference_category_id]); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($productObj->name); ?></td>
                  <td>
                    <?php if ($productObj->flag_active == 'Y') { ?>
                    <span class='label label-success'>上架</span>
                    <?php } else { ?>
                    <span class='label label-warning'>下架</span>
                    <?php } ?>
                  </td>
                  <td><?php echo $this->tag->image(array(stripslashes($productObj->img), 'width' => '100')); ?></td>
                  <td>
                    <div class='text-right'>
                      <?php echo $this->tag->linkTo(array('product/edit/' . $productObj->product_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('product/delete/' . $productObj->product_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>