<?php echo $this->getContent(); ?>
<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='title'>最新消息列表</div>
        <div class='pull-right'>
          <?php echo $this->tag->linkTo(array('news/create', '<i class=\'icon-pencil\'></i>新增', 'class' => 'btn', 'style' => 'margin-bottom:5px')); ?>
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    創建時間
                  </th>
                  <th>
                    發佈日期
                  </th>
                  <th>
                    標題
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($newsListObj as $listNews) { ?>

                <tr>
                  <td><?php echo $this->escaper->escapeHtml($listNews->news_id); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($listNews->create_time); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($listNews->publish_time); ?></td>
                  <td><?php echo $this->escaper->escapeHtml($listNews->title); ?></td>
                  <td>
                    <div class='text-right'>
                    <?php echo $this->tag->linkTo(array('news/edit/news_id/' . $listNews->news_id, '<i class=\'icon-edit\'></i>', 'class' => 'btn btn-success btn-xs')); ?>
                      <?php echo $this->tag->linkTo(array('news/delete/news_id/' . $listNews->news_id, '<i class=\'icon-trash\'></i>', 'class' => 'btn btn-danger btn-xs')); ?>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>