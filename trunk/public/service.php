<?php
// If you haven't already, require autoload.php from the vendor directory
include_once('vendor/autoload.php');

$SUBDOMAIN_NAME = 'qbon';
$API_KEY = 'xna2Xs3gsahLJcG7CHTBFw';
$API_SECRET = 'EQ3OBAyNE1H1kmEwqlz0f5ARRM1ZEAZi7ImTkVAqbk';
$URL = '';

$client = new \UserVoice\Client($SUBDOMAIN_NAME, $API_KEY, $API_SECRET, array('callback' => $URL));
$token = $client->login_as('qbon@hiiir.com');

if(isset($_POST["email"])){
    $email = trim($_POST["email"]);
}
if(isset($_POST["mobile"])){
    $mobile = trim($_POST["mobile"]);
}
$questionType = trim($_POST["questionType"]);
$content = trim($_POST["content"]);
$latitude = trim($_POST["latitude"]);
$longitude = trim($_POST["longitude"]);

$message = "";

//若給的email跟mobile都不存在
if(empty($email) && empty($mobile)){
    if(empty($email)){
        $message .= " email";
    }
    
    if(empty($mobile)){
        $message .= " mobile";
    }
    
    $returninfo = array(
            "status"  => "fail",
            "code"    => "SERVICE-001",
            "message" => "無法判斷身分！",
            "detail"  => $message
    );
    print_r(json_encode($returninfo));
    exit();    
}

//判斷email是否正確
if(!empty($email)){    
    //若信箱錯誤，判斷有沒有電話，組成假信箱
    if(!checkemail($email)){
        if(!empty($mobile)){
            if(!checkemail($mobile)){
                $email = $mobile."@hiiir.com";
            }else{
                $email = $mobile;
            }
        }else{
            $returninfo = array(
                    "status"  => "fail",
                    "code"    => "SERVICE-002",
                    "message" => "信箱格式錯誤",
                    "detail"  => ""
            );
            print_r(json_encode($returninfo));
            exit();            
        }
    }
}elseif(!empty($mobile)){
    if(!checkemail($mobile)){
        $email = $mobile."@hiiir.com";
    }else{
        $email = $mobile;
    }
}


if(empty($questionType)||empty($content)||empty($latitude)||empty($longitude)){
    if(empty($questionType)){
        $message .= " questionType";    
    }
    if(empty($content)){
        $message .= " content";    
    }
    if(empty($latitude)){
        $message .= " latitude";        
    }
    if(empty($longitude)){
        $message .= " longitude";
    }
    
    $returninfo = array(
            "status"  => "fail",
            "code"    => "SERVICE-003",
            "message" => "傳送參數不完全！",
            "detail"  => $message
    );
    print_r(json_encode($returninfo));
    exit();
}

$result = $client->post("/api/v1/tickets.json", array(
        'email' => $email,
        'ticket' => array(
                'state' => 'open',
                'subject' => '客服問題',
                'message' => 
                "信箱：".$email.
                "電話：".$mobile.
                "問題類型：".$questionType.
                "問題內容：".$content.
                "緯度：".$latitude.
                "經度：".$longitude
        )
));

$returninfo = array(
        "status"  => "ok",
        "code"    => "200",
        "message" => "客服問題已送出！",
        "detail"  => ""	
);

print_r(json_encode($returninfo));

function checkemail($email){
    if (preg_match('/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/', $email) == true) {
        return true;
    }else{
        return false;
    }    
}
?>
