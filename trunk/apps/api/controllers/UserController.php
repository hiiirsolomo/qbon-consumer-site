<?php

namespace Qbon\Api\Controllers;

use Qbon\Models\User,
	Mike\Util;

class UserController extends ControllerBase
{

	public function indexAction()
	{

	}

	public function getAction()
	{
		$param["user_id"] = Util::sqlInjectionAnti($_POST["user_id"]);

		$this->view->setVar("description", "test test tets");
		$this->sendData();
		// $this->sendError(404, 404, "not found");
		return;


		if (Util::paramIsMissing($param)) {
		    return errorResponse('400', '缺少必要參數');
		}

		// $userObj = User::findFirst($auth['id']);

		// $db = DBConnect("Master");
		// if(!$db) { return errorResponse('500'); }
		// $Sql = "SELECT COUNT(0) AS `count` FROM tb_user WHERE flag_delete = 1 AND user_guid = '{$param['user_id']}' LIMIT 1";
		// $query = $db->query($Sql);
		// $result = $query->fetch();

		// if ($result["count"] == 0) {
		//     return errorResponse('405', '查無user_id');
		// }

		// $Sql = "INSERT INTO tb_restart_log(user_guid, update_time) VALUES('{$param['user_id']}', NOW())";
		// $db->exec($Sql);
		// unset($db);

		// $output['result'] = "success";
		// $output['errcode'] = "0";
		// $output['ver'] = IOS_VER;
		// $output['block'] = IOS_BLOCK;
		// $output['ver_i'] = ANDROID_VER;
		// $output['block_i'] = ANDROID_BLOCK;
		// return $output;
		// $output = array("errcdoe" => 404, "msg" => "not found");

		// echo json_encode($output);
		// $this->view->setVar("output", $output);
	}

	public function albumAction($albumId, $albumName)
	{

		$key = 'album'.$albumId;

		$exists = $this->view->getCache()->exists($key);
		if (!$exists) {

			$album = Albums::findFirst(array(
				'id = ?0', 'bind' => array($albumId)
			));
			if ($album == false) {
				return $this->dispatcher->forward(array(
					'controller' => 'index',
					'action' => 'index'
				));
			}

			//Top tags
			$phql = 'SELECT t.name
			FROM Qbon\Models\AlbumsTags at
			JOIN Qbon\Models\Tags t
			WHERE
			at.albums_id = '.$album->id.'
			LIMIT 10';
			$tags = $this->modelsManager->executeQuery($phql);

			//Top albums
			$phql = 'SELECT
			al.id,
			al.name,
			al.uri
			FROM Qbon\Models\Albums al
			WHERE
			al.id <> '.$album->id.' AND
			al.artists_id = '.$album->artists_id.' AND
			al.playcount > 25000
			ORDER BY al.playcount DESC
			LIMIT 5';
			$relatedAlbums = $this->modelsManager->executeQuery($phql);

			$album->loadPalette();

			$this->view->setVar('album', $album);
			$this->view->setVar('relatedAlbums', $relatedAlbums);
			$this->view->setVar('artist', $album->getArtist());
			$this->view->setVar('tags', $tags);
			$this->view->setVar('tracks', $album->getTracks());
			$this->view->setVar('photo', $album->getPhoto('extralarge'));

		}

		$this->view->cache(array("key" => $key));

	}

	 public function tagAction($tagName, $page=1)
	{

		$page = $this->filter->sanitize($page, 'int');
		if ($page < 1) {
			$page = 1;
		}

		$tagItem = Tags::findFirst(array(
			'name = ?0', 'bind' => array($tagName)
		));
		if ($tagItem == false) {
			return $this->dispatcher->forward(array(
				'controller' => 'index',
				'action' => 'index'
			));
		}

		$key = 'tag'.$tagItem->id.'p'.$page;

		$exists = $this->view->getCache()->exists($key);
		if (!$exists) {

			//Top albums
			$phql = 'SELECT
			al.id,
			al.name,
			ar.uri,
			ar.id as artist_id,
			ar.name as artist,
			ap.url
			FROM Qbon\Models\Albums al
			JOIN Qbon\Models\Artists ar
			JOIN Qbon\Models\AlbumsTags at
			JOIN Qbon\Models\AlbumsPhotos ap
			WHERE
			ap.type = "large" AND
			at.tags_id = '.$tagItem->id.'
			ORDER BY al.playcount DESC
			LIMIT 30
			OFFSET '.(($page-1)*30);
			$albums = $this->modelsManager->executeQuery($phql);

			$this->view->setVar('tagItem', $tagItem);
			$this->view->setVar('albums', $albums);
			$this->view->setVar('page', $page);
			$this->view->setVar('prev', $page == 1 ? 0 : $page-1);
			$this->view->setVar('next', count($albums) == 30 ? $page+1 : 0);

		}

		$this->view->cache(array("key" => $key));

	}

	public function searchAction()
	{

		//Top albums
		$phql = 'SELECT
		ar.id,
		ar.name,
		ar.uri,
		ap.url
		FROM Qbon\Models\Artists ar
		JOIN Qbon\Models\ArtistsPhotos ap
		WHERE
		ap.type = "large" AND
		ar.name LIKE ?0
		ORDER BY ar.listeners DESC
		LIMIT 30';

		$artists = $this->modelsManager->executeQuery($phql, array(
			'%'.preg_replace('/[ ]+/', '%', $this->request->getPost('s')).'%'
		));

		$this->view->setVar('artists', $artists);

	}

	public function popularAction()
	{

		$key = 'popular';

		$exists = $this->view->getCache()->exists($key);
		if (!$exists) {

			//Top albums
			$phql = 'SELECT
			al.id,
			al.name,
			ar.uri,
			ar.id as artist_id,
			ar.name as artist,
			ap.url
			FROM Qbon\Models\Albums al
			JOIN Qbon\Models\Artists ar
			JOIN Qbon\Models\AlbumsPhotos ap
			WHERE
			ap.type = "large"
			ORDER BY al.playcount DESC
			LIMIT 30';
			$albums = $this->modelsManager->executeQuery($phql);

			$this->view->setVar('albums', $albums);

			//Top tags
			$phql = 'SELECT t.name, COUNT(*)
			FROM Qbon\Models\Tags t
			JOIN Qbon\Models\AlbumsTags at
			GROUP BY 1
			HAVING COUNT(*) > 50
			LIMIT 14';
			$tags = $this->modelsManager->executeQuery($phql);

			$this->view->setVar('tags', $tags);

		}

		$this->view->cache(array("key" => $key));
	}

	/**
	 * This action handles the /charts route
	 */
	public function chartsAction()
	{

		$key = 'charts';

		$exists = $this->view->getCache()->exists($key);
		if (!$exists) {

			$tagGenres = array(
				'pop', 'rock', 'rap',
				'rnb', 'electronic', 'alternative',
				'folk', 'country', 'hip-hop',
				'dance', 'chillout', 'trip-hop',
				'metal', 'ambient', 'soul',
				'jazz', 'latin', 'punk'
			);

			$charts = array();
			foreach ($tagGenres as $genre) {

				$tag = Tags::findFirst(array(
					'name = ?0', 'bind' => array($genre)
				));
				if ($tag == false) {
					continue;
				}

				//Top albums
				$phql = 'SELECT
				al.id,
				al.name,
				ar.uri,
				ar.id as artist_id,
				ar.name as artist,
				ap.url
				FROM Qbon\Models\Albums al
				JOIN Qbon\Models\Artists ar
				JOIN Qbon\Models\AlbumsTags at
				JOIN Qbon\Models\AlbumsPhotos ap
				WHERE
				ap.type = "small" AND
				at.tags_id = '.$tag->id.'
				ORDER BY al.playcount DESC
				LIMIT 10';
				$charts[$tag->name] = $this->modelsManager->executeQuery($phql);
			}

			$this->view->setVar('charts', $charts);

		}

		$this->view->cache(array("key" => $key));
	}

}
