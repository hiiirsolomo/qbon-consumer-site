<?php

namespace Qbon\Api\Controllers;


class ControllerBase extends \Phalcon\Mvc\Controller
{

    protected function initialize()
    {

    }


    /**
     *  send header
     *
     *  送出標頭
     *
     *  @param [in] $httpCode Parameter_Description
     */
    protected function sendHeader($httpCode = "200")
    {
        if (ENV == "development") {
            $this->response->setHeader("Access-Control-Allow-Origin", "*");
            $this->response->setHeader("Access-Control-Allow-Headers", "api_key, content-type, AUTHORIZATION, authorization");
            $this->response->setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            $this->response->setHeader("Pragma", "no-cache, must-revalidate");
            $this->response->setHeader("Expires", "0");
        }

        $this->response->setHeader("Content-Type", $this->getContentType(API_OUTPUT_TYPE));

        $headerAry = include LIB_ROOT . "HttpStatus.php";
        $this->response->setStatusCode($httpCode, $headerAry[$httpCode]);
        $this->response->send();
    }

    // 設定 header, 成功固定值 ok, 0,
    protected function sendData()
    {
        $this->sendHeader();
        $this->view->setVar("result", "OK");
        $this->view->setVar("errcode", "0");

        $output = $this->view->getParamsToView();
        $output = \Mike\Util::excludeNull($output);
        $this->view->setVar("output", $output);

    }

    protected function sendError($httpCode, $errCode, $msg)
    {
        $this->sendHeader($httpCode);
        $this->view->setVar("result", $msg);
        $this->view->setVar("errcode", $errCode);

        $output = $this->view->getParamsToView();
        $output = \Mike\Util::excludeNull($output);
        $this->view->setVar("output", $output);
    }



    protected function getContentType($apiOutputType)
    {
        $typeAry = array(
                    "JSON" => "application/json",
                    "XML" => "application/xml",
                    "ARRAY" => "text/plain",
                    );
        return $typeAry[$apiOutputType];
    }



}