<?php

namespace Qbon\Api;

class Module
{

	public function registerAutoloaders()
	{
		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Qbon\Api\Controllers' => __DIR__.'/controllers/'
		));

		$loader->register();
	}

	public function registerServices($di)
	{

		/**
		 * Setting up the view component
		 */
		$di->set('view', function() {

			$view = new \Phalcon\Mvc\View();
			$view->setViewsDir(__DIR__.'/views/');
			$view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);

			if (API_OUTPUT_TYPE == "JSON") {
			    $view->setMainView('json');
			} elseif (API_OUTPUT_TYPE == "XML") {
			    $view->setMainView('xml');
			} elseif (API_OUTPUT_TYPE == "ARRAY") {
			    $view->setMainView('array');
			}


			// $view->disable();
			return $view;

		});



	}

}
