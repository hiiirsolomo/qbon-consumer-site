<section>
            <article id="main">
                {{ partial("partials/indexNews")}}

                {{ partial("partials/indexApp") }}


                <div id="focus">
                    <div id="focus-data">
                        <div id="focus-what-is-qbon">
                            <div class="box-title">
                                什麼是<b>Qbon</b>？
                            </div>
                            <p>
                                為您蒐集食衣住行育樂，各類優惠票券與好康資訊，更是隨取即用的優惠接收器，任何好康優惠都逃不出你的手掌心
                            </p>
                            <a href="http://goo.gl/h4aqL7" class="btn-pink app_down_btn" style="float: right;width: 130px; height: 24px; margin: 10px 5px 2px 5px;">
                        <img src="/frontpublic/images/btn_download_google_wd.png" alt="" ></a>
                            <a href="http://goo.gl/Wl6w6h" class="btn-pink app_down_btn" style="float: right;width: 130px; height: 24px; margin: 10px 5px 2px 5px;">
                        <img src="/frontpublic/images/btn_download_apple_wd.png" alt=""> </a>
                        </div>

                        <div id="focus-where">
                            <div class="box-title">
                                哪裡有？
                            </div>
                            <div id="where-box">
                                <ul id="where-nav">
                                    <li class="nav-1">
                                        下載聯盟Apps中任一 app
                                        <img src="/frontpublic/images/img_focus-where_1.jpg" alt="下載聯盟Apps中任一 app">
                                    </li>
                                    <li class="nav-2">
                                        開啟 app，可以找到Qbon Logo
                                        <img src="/frontpublic/images/img_focus-where_2.jpg" alt="開啟 app，可以找到Qbon Logo">
                                    </li>
                                    <li class="nav-3">
                                        按下Qbon Logo，就可以開啟Qbon優惠牆
                                        <img src="/frontpublic/images/img_focus-where_3.jpg" alt="按下Qbon Logo，就可以開啟Qbon優惠牆">
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                {{ partial("partials/adds") }}
                

                <div id="how">
                    <div class="box-title">
                        <b>
                            怎麼領優惠？
                        </b>
                    </div>
                    <div class="tutorial-box how-1">
                        <span>
                            Step 1.
                        </span>
                        <h3>
                            在任一聯盟APP中開啟Qbon 優惠牆
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_how1.png" alt="">
                    </div>
                    <div class="tutorial-box how-2">
                        <span>
                            Step 2.
                        </span>
                        <h3>
                            在優惠牆上找到你想要的優惠票券
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_how2.png" alt="">
                    </div>
                    <div class="tutorial-box how-3">
                        <span>
                            Step 3.
                        </span>
                        <h3>
                            按下立即領取，優惠券成功加入我的口袋
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_how3.png" alt="">
                    </div>
                </div>


                <div id="redeem">
                    <div class="box-title">
                        <b>
                            領到的優惠該如何兌換？
                        </b>
                    </div>
                    <div class="tutorial-box redeem-1">
                        <span>
                            Step 1.
                        </span>
                        <h3>
                            於聯盟app中開啟Qbon優惠牆或下載QbonWallet
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_redeem1.png" alt="">
                    </div>
                    <div class="tutorial-box redeem-2">
                        <span>
                            Step 2.
                        </span>
                        <h3>
                            登入後點選『我的口袋』，找到想要兌換的那張票券
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_redeem2.png" alt="">
                    </div>
                    <div class="tutorial-box redeem-3">
                        <span>
                            Step 3.
                        </span>
                        <h3>
                            由店家確認點擊『馬上兌換』
                        </h3>
                        <img src="/frontpublic/images/img_tutorial_redeem3.png" alt="">
                    </div>
                </div>
            </article>
        </section>