<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8" />
    <title>Qbon 優惠牆</title>
    <meta name="keywords" content="">
    <meta name="description" content=""/>
    <link rel="stylesheet" type="text/css" href="/frontpublic/stylesheets/reset.css" />
    <link rel="stylesheet" type="text/css" href="/frontpublic/stylesheets/common-use.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="/js/jquery.twzipcode.min.js"></script>
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
        <link href="/frontpublic/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
    function checkserAgent(){
        var userAgentInfo=navigator.userAgent;
        var userAgentKeywords=new Array(¡§Android", ¡§iPhone" ,"SymbianOS", ¡§Windows Phone", ¡§iPad", ¡§iPod", ¡§MQQBrowser");
        var flag=false;
        if(userAgentInfo.indexOf(¡§Windows NT")==-1){
        flag=true;
        }
        return flag;
    }
/*
    if(checkserAgent()){
        document.location.href="http://www.qbon.com.tw/mobile";
    }
*/
    </script>
</head>
<body>
    <div id="container">
        <header>
            <h1><a href="/">Qbon 優惠牆</a></h1>
            {{ partial("partials/priorityLink") }}
        </header>

        {{ content() }}

        <footer>
            <div id="footer_data">
                <div id="footer-platform">
                    <div class="platform-box dev">
                        <h4>
                            你是開發者？
                        </h4>
                        <p>
                            想讓你開發的app加入Qbon聯盟嗎？快上Qbon 開發者平台～
                        </p>
                        <a href="http://developer.qbon.com.tw">
                            >立刻前往
                        </a>
                    </div>

                    <div class="platform-box mer">
                        <h4>
                            你是商家？
                        </h4>
                        <p>
                            想讓你的商品在 Qbon 優惠牆中曝光嗎？快到商家平台看看加入辦法～
                        </p>
                        <a href="http://merchant.qbon.com.tw">
                            >立刻前往
                        </a>
                    </div>
                </div>

                <ul>
                    <li><a href="mailto:qbon@hiiir.com">聯絡我們</a></li>
                    <li><a href="https://sso.hiiir.com/mobile/signup/rule">隱私權條款與個資利用宣告</a></li>
                </ul>
                <div id="footer-logo">
                    Hiiir X 遠傳
                </div>
            </div>
        </footer>
    </div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50338452-2', 'qbon.com.tw');
  ga('send', 'pageview');

</script>
</html>
