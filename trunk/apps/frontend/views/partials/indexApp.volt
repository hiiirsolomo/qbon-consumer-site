
<div id="index-apps">
    <div class="apps-data">
        <h2>
            聯盟Apps
        </h2>
        <p>
            下載Qbon聯盟旗下各種Apps，豐富你的生活，還能隨時隨地掌握最新或離自己最近的優惠！
        </p>
        <a href="{{ url("apps") }}" class="btn-pink">
            更多聯盟Apps
        </a>
    </div>
    {% if indexAppObj %}

    <div class="grid-lists">
    {% for indexAppRow in indexAppObj %}
        <div class="gird">
            <div class="gird-img">
                <img src="{{ url(indexAppRow.img) }}" width="110" height="110" alt="">
            </div>
            <div class="gird-data">
                <h3>
                    {{ indexAppRow.name }}
                </h3>
                <p>
                </p>
            </div>
            <div class="download-box">
                <a href="" class="btn-pink download"></a>
                <div class="download-pink">
                    {% if indexAppRow.apple_link !="" %}
                    <a href="{{ indexAppRow.apple_link }}" class="apple-store" title="apple store">apple store</a>
                    {% endif %}
                    
                    {% if indexAppRow.android_link !="" %}
                    <a href="{{ indexAppRow.android_link }}" class="google-play" title="google play">google play</a>
                    {% endif %}
                </div>
            </div>
        </div>
    {% endfor %}
    </div>
    {% else %}
    目前無資料
    {% endif %}
</div>