<div class="path">首頁   > <a href="/apps"> 聯盟Apps</a></div>

<div class="main_bg">
    <div class="title_block left">

        <div class="title_alliance"></div>

    </div> 

    <div class="bg_white">
        {% if appsObj %}
        {% for appsRow in appsObj %}

        <div class="app_block">

            <div class="app_block_img left">
                <img src="{{ url(appsRow.img) }}" width="90" height="90" alt="">
            </div>
            <div class="app_block_wording left">
                <ul>
                    <li>
                        <h1>{{ appsRow.name|e }}</h1>
                    </li>
                    <li>
                        {{ appsRow.desc|e }}
                    </li>
                    <li>
                        {% if appsRow.android_link != "" %}
                        <a href="{{ appsRow.android_link }}" class="btn-pink app_down_btn">
                        <img src="/frontpublic/images/btn_download_google_wd.png" alt=""></a>
                        {% endif %} 
                        {% if appsRow.apple_link != "" %}
                        <a href="{{ appsRow.apple_link }}" class="btn-pink app_down_btn">
                        <img src="/frontpublic/images/btn_download_apple_wd.png" alt=""> </a>
                        {% endif %}
                    </li>
                    </ul>
            </div>
        </div>
                {% endfor %}
                {% else %}
                目前無資料
                {% endif %}


            </div>

            <div class="page_bar">

                <div class="page">
                    {% if pageNumbers %}

                    {% if currentPage > 1 %}
                    <a href="/apps/p/{{ currentPage-1 }}">上一頁 >> </a>
                    {% endif %}

                    {% for index in 1..pageNumbers %}

                    {% if index == currentPage %}
                    <a href="/apps/p/{{ index }}" class="Active">{{ index }}</a>
                    {% else %}
                    <a href="/apps/p/{{ index }}" class="">{{ index }}</a>
                    {% endif %}

                    {% endfor %}

                    {% if currentPage < pageNumbers %}
                    <a href="/apps/p/{{ currentPage+1 }}">下一頁 >> </a>
                    {% endif %}

                    {% endif %}


                </div>

            </div>


            {{ partial("partials/adds2") }}

        </div>