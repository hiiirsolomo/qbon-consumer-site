<div class="path">首頁   > <a href="#"> 個人資料</a></div>
<div class="main_bg">

    <div class="file_block">
      <div class="title_block_s left">
        <div class="title_file"></div>
    </div> 
    
    <div class="account_block left">{{ userEmail }}</div>
    <div class="btn_pocket left"><a href="{{ url("wallet") }}" class="{{ edArray["wallet"] }}"></a></div>
    <div class="btn_chfile left"><a href="{{ url("profile") }}" class = "{{ edArray["profile"] }}"></a></div> 
    <div class="btn_chpass left"><a href="{{ url("password") }}" class="{{ edArray["password"] }}"></a></div> 
    
    <div class="borterline left"></div>
    
    
    
    {{ partial("partials/adds3") }}
    
    
    
</div>


<div class="bg_white_file left">

    {% if redeemed %}
    {% set didRedeemed = "actived" %}
    {% else %}
    {% set unRedeemed = "actived"%}
    {% endif %}
    <div class="tab left">
        <div class="tab_unexchange left"><a href="{{ url("wallet/redeemed/0") }}" class="{{ unRedeemed }}"></a></div>
        <div class="tab_exchange left"><a href="{{ url("wallet/redeemed/1") }}" class="{{ didRedeemed }}"></a></div>
    </div>
    
    {% if walletObj %}
    {% for walletRow in walletObj %}

    <div class="exchange_block left">

     <div class="exching_img left">
         <img src="{{ walletRow.image }}" width="115" height="70" alt="">
     </div>

     <div class="exchange left">

        <ul>
            <li><h1>{{ walletRow.offerName }}</h1></li>
            
            <li>{{ walletRow.detail }}</li>
            {% if walletRow.offerType == "COUPON" %}
            <li><div class="icon_excg coupon left"></div>優惠券</li>
            {% elseif walletRow.offerType == "MISSION" %}
            <li><div class="icon_excg mission left"></div>任 務</li>
            {% elseif wallerRow.offerType == "VOUCHER" %}
            <li><div class="icon_excg gift left"></div>商品券</li>
            {% endif %}
        </ul>
    </div>
</div>



{% endfor %}
{% else %}
目前無資料
{% endif %}


<div class="page_bar" style="padding-left:20px;">

    <div class="page">

        {% if countPages %}

        {% if currentPage > 1 %}
        <a href="/wallet/redeemed/{{ redeemed }}/p/{{ currentPage-1 }}">上一頁 >> </a>
        {% endif %}

        {% for index in 1..countPages %}

        {% if index == currentPage %}
        <a href="/wallet/redeemed/{{ redeemed }}/p/{{ index }}" class="Active">{{ index }}</a>
        {% else %}
        <a href="/wallet/redeemed/{{ redeemed }}/p/{{ index }}" class="">{{ index }}</a>
        {% endif %}

        {% endfor %}

        {% if currentPage < countPages %}
        <a href="/wallet/redeemed/{{ redeemed }}/p/{{ currentPage+1 }}">下一頁 >> </a>
        {% endif %}

        {% endif %}
    </div>

</div>

</div>
</div>


