
<div class="path">首頁   > <a href="/news"> 最新消息</a></div>

<div class="main_bg">
    <div class="title_block left">

        <div class="title_news"></div>

    </div> 

    <div class="bg_white">

        <div class="news_block">

            <ul>
                {% if newsObj %}
                {% set i = 1 %}
                {% for newsRow in newsObj %}
                {% if i % 2 == 1 %}
                <li class="nbg_g">
                    {% else %}
                    <li class="nbg_w">
                        {% endif %}
                        <div class="news_day">{{ newsRow.publish_time }}</div>
                        <div class="news_wording">
                            {{ newsRow.content|nl2br }}
                        </div>
                    </li>
                    {% set i += 1 %}
                    {% endfor %}
                    {% else %}
                    目前無資料
                    {% endif %}

                </ul>
                
            </div> 

        </div>

        <div class="page_bar">

            <div class="page">
                {% if pageNumbers %}

                {% if currentPage > 1 %}
                <a href="/news/p/{{ currentPage-1 }}">上一頁 >> </a>
                {% endif %}

                {% for index in 1..pageNumbers %}

                {% if index == currentPage %}
                <a href="/news/p/{{ index }}" class="Active">{{ index }}</a>
                {% else %}
                <a href="/news/p/{{ index }}" class="">{{ index }}</a>
                {% endif %}

                {% endfor %}

                {% if currentPage < pageNumbers %}
                <a href="/news/p/{{ currentPage+1 }}">下一頁 >> </a>
                {% endif %}

                {% endif %}
            </div>

        </div>


        {{ partial("partials/adds2") }}

    </div>