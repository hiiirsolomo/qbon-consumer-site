
{{ zipCodeJs }}
<div class="main_bg">

  <div class="file_block">
    <div class="title_block_s left">
      <div class="title_file"></div>
    </div> 

    <div class="account_block left">{{ userEmail }}</div>
    <div class="btn_pocket left"><a href="{{ url("wallet") }}" class="{{ edArray["wallet"] }}"></a></div>
    <div class="btn_chfile left"><a href="{{ url("profile") }}" class = "{{ edArray["profile"] }}"></a></div> 
    <div class="btn_chpass left"><a href="{{ url("password") }}" class="{{ edArray["password"] }}"></a></div> 

    <div class="borterline left"></div>



    {{ partial("/partials/adds3") }}



  </div>


  <div class="bg_white_s left" style="padding-top:30px;">
  <form action="/profile/update" id="pfform" method="post">
    <div class="password">
      <ul>
        <li>
          <strong>用戶名稱</strong><br>
          <label for="textfield1"></label>
          <input name="name" type="text" class="passinput" id="textfield1" value="{{ memberObj.name }}">
        </li>
        <li><strong>地區</strong><br>

         <div id="twzipcode">
          <div data-role="zipcode" style="display: none"></div>  <!--自訂郵遞區號容器，以及套用 .zipcode 樣式-->
          <div data-role="county" style="display: inline"></div> <!--自訂縣市選單容器，以及套用 .county 樣式-->
          <div data-role="district" style="display: inline"></div> <!--自訂鄉鎮市區選單容器，以及套用 .district 樣式-->
        </div>

      </li>
      <li><strong>Email</strong><br>
        <label for="textfield"></label>
        <input type="text" class="passinput" id="textfield" value="{{ memberObj.email }}" disabled>
      </li>
      <li>
        <strong>手機號碼</strong><br>
        <label for="textfield"></label>
        <input type="text" class="passinput" id="textfield" value="{{ memberObj.mobile }}" disabled>
      </li>
    </ul>
  </div>
</form>
  <a href="javascript:submitform();" class="btn-pink file_btn">
    儲存修改</a>



  </div>




</div>