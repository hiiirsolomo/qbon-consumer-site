<div class="main_bg">

  <script> 
    function submitform() {
      var txt = $("#text1").val();
      var txt2 = $("#text2").val();
      var txt3 = $("#text3").val();
      re = /[a-zA-Z0-9]/;
      if(txt3 == ""){
        alert("請輸入原密碼哦！");
      }
      if(!re.test(txt)){
       alert("有非英文及數字的字喔!"); 
     }else if(txt !== txt2){
      alert("確認密碼不同噢！");
    }else{
      $("#pwform").submit();
    }

  } 
</script> 

<div class="file_block">
  <div class="title_block_s left">
    <div class="title_file"></div>
  </div> 

  <div class="account_block left">{{ userEmail }}</div>
  <div class="btn_pocket left"><a href="{{ url("wallet") }}" class="{{ edArray["wallet"] }}"></a></div>
  <div class="btn_chfile left"><a href="{{ url("profile") }}" class = "{{ edArray["profile"] }}"></a></div> 
  <div class="btn_chpass left"><a href="{{ url("password") }}" class="{{ edArray["password"] }}"></a></div> 



  <div class="borterline left"></div>



  {{ partial("partials/adds3") }}



</div>


<div class="bg_white_s left" style="padding-bottom:70px;">
  <form id="pwform" action="{{ url("password/update") }}" method="post">
    <div class="password">
      <ul>
        <li><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="14%">舊密碼</td>
            <td width="86%"><label for="textfield"></label>
              <input name="originalPassword" type="password" class="passinput" id="text3" value=""></td>
            </tr>
          </table>
        </li>
        <li><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="14%">新密碼</td>
            <td width="86%"><input type="password" name="newPassword" id="text1" class="passinput"></td>
          </tr>
        </table></li>
        <li><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="14%">再次輸入</td>
            <td width="86%"><input type="password" name="newPassword2" id="text2" class="passinput"></td>
          </tr>
        </table></li>
      </ul>
    </div>
  </form>

  <a href="javascript: submitform()" class="btn-pink file_btn">
    確定送出</a>

    <a href="javascript: history.back(1)" class="btn-pink fileun_btn">
      取 消</a>


    </div>




  </div>