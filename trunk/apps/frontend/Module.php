<?php

namespace Qbon\Frontend;

class Module
{

	public function registerAutoloaders()
	{

		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Qbon\Frontend\Controllers' => __DIR__.'/controllers/',
			'Qbon\Models' => __DIR__.'/../../common/models/',
			'Qbon\Components\Palette' => __DIR__.'/../../common/library/Palette/',
		));

		$loader->register();
	}

	public function registerServices($di)
	{

		$di->set('view', function() {

			$view = new \Phalcon\Mvc\View();

			$view->setViewsDir(__DIR__.'/views/');

			$view->setTemplateBefore('main');

			$view->registerEngines(array(
				".volt" => function($view, $di) {

					$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

					$volt->setOptions(array(
						"compiledPath" => __DIR__."/../../var/volt/",
						"compiledExtension" => ".php"
					));

					return $volt;
				}
			));

			return $view;

		});

		// module session 拆分不共用
		$di->set("session", function() use($di) {
			$router = $di->get('router');
			$session = new \Phalcon\Session\Adapter\Files(array('uniqueId' => $router->getModuleName()));
			$session->start();
			return $session;
		});


	}

}
