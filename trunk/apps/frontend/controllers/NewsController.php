<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - NewsController extends ControllerBase
 */
namespace Qbon\Frontend\Controllers;
use Qbon\Models\News;
class NewsController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_news' . $date;
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            $newsCountObj = News::find("flag_active='Y'");
            $newsNumber = count($newsCountObj);
            $page = $this->dispatcher->getParam("p", "int", 1);

            $pageLimit = $page ? ($page - 1) * 5 : 0;
            $newsObj = News::find(array(
                "conditions" => "flag_active = ?1",
                "bind" => array(
                    "1" => "Y"
                ) ,
                "order" => "publish_time DESC",
                "limit" => array(
                    "number" => 5,
                    "offset" => $pageLimit
                )
            ));
            
            $pageNumbers = ceil($newsNumber / 5);

            $this->view->setVar("pageNumbers", $pageNumbers);
            $this->view->setVar("currentPage", $page);
            $this->view->setVar("newsObj", $newsObj);
        }
        $this->getMenuType();
        $this->getAdds();
        $this->view->cache(array(
            "key" => $key
        ));
    }
}
