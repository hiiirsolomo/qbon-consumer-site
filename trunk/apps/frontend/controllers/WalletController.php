<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - WalletController extends ControllerBase
 */
namespace Qbon\Frontend\Controllers;
class WalletController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_wallet' . $date;
        $this->getMenuType();
        
        $url = API_URL . "api/dev0.1/consumer/wallet/get";
        $redeemed = $this->dispatcher->getParam("redeemed", "int", "0");
        $page = $this->dispatcher->getParam("p", "int", 1);
        $accountId = $this->session->get("user_account_id");
        $pageStart = $page ? ($page - 1) * 3 : 0;
        $postArray = array(
            "redeemed" => $redeemed,
            "accountId" => $accountId,
            "start" => $pageStart,
            "limit" => 3
        );
        $result = $this->getCurlResponse($url, $this->session->get("user_token") , $postArray);
        $walletObj = $result->items;
        $countNum = $result->countTotal;
        $countPages = ceil($countNum / 3);

        $this->view->setVar("countPages", $countPages);
        $this->view->setVar("currentPage", $page);

        $this->getAdds();
        
        $this->view->setVar("redeemed", $redeemed);
        $this->view->setVar("walletObj", $result->items);
        $this->view->cache(array(
            "key" => $key
        ));
    }
}

