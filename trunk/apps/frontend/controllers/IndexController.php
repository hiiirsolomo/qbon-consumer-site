<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * - signoutAction()
 * Classes list:
 * - IndexController extends ControllerBase
 */
namespace Qbon\Frontend\Controllers;
use Qbon\Models\News;
use Qbon\Models\Product;
class IndexController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_index' . $date;
        
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            $newsObj = News::findFirst(array(
                "conditions" => "flag_active = ?1",
                "bind" => array(
                    "1" => "Y"
                ) ,
                "order" => "publish_time DESC"
            ));
            $appsObj = Product::find(array(
                "conditions" => "flag_active = ?1",
                "bind" => array(
                    "1" => "Y"
                ) ,
                "limit" => "4"
            ));
            $this->getAdds();
            $this->view->setVar("indexAppObj", $appsObj);
            $this->view->setVar("indexNewsObj", $newsObj);
        }
        $this->getMenuType();
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function signoutAction()
    {
        $this->session->remove("user_account_id");
        $this->session->remove("user_token");
        $this->session->remove("user_name");
        $this->session->remove("user_email");
        return $this->response->redirect("");
    }
}

