<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * - updateAction()
 * Classes list:
 * - ProfileController extends ControllerBase
 */
namespace Qbon\Frontend\Controllers;
class ProfileController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_profile' . $date;
        $this->getMenuType();
        $url = API_URL . "api/1.0/account/profile/get";
        $result = $this->getCurlResponse($url, $this->session->get("user_token"));
        
        if ($result->member->zipCode) {
            $zipCodeJs = <<<END
            <script>
              $(function(){
                $('#twzipcode').twzipcode({
                  zipcodeSel: {$result->member->zipCode},
                  readonly: false
                });
              });
              function submitform() {
                $("#pfform").submit();
              } 
            </script>
END;
            
            
        } else {
            $zipCodeJs = <<<END
            <script>
              $(function(){
                $('#twzipcode').twzipcode({});
              function submitform() {
                $("#pfform").submit();
              } 
            </script>
END;
            
        }
        $this->view->setVar("zipCodeJs", $zipCodeJs);
        $this->view->setVar("memberObj", $result->member);
        $this->getAdds();
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function updateAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_profile_update' . $date;
        $param = array();
        $param["name"] = $this->request->getPost("name", "striptags");
        $param["zipCode"] = $this->request->getPost("zipcode", "int");
        

        // $param["email"] = $this->request->getPost("email", "email");
        // $param["mobile"] = $this->request->getPost("mobile", "int");
        $url = API_URL . "api/1.0/account/profile/update";
        $result = $this->getCurlResponse($url, $this->session->get("user_token") , $param);
        
        if ($result->code == "200") {
            echo <<<END
            <script>
                alert("修改成功。");
                history.back(1);
            </script>
END;
            
            
        } else {
            echo <<<END
            <script>
                alert("修改失敗。");
                history.back(1);
            </script>
END;
            
            
        }
    }
}

