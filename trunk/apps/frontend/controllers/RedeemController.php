<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - IndexController extends ControllerBase
 */
namespace Qbon\Frontend\Controllers;
class RedeemController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_redeem' . $date;
        $this->getMenuType();
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
}

