<?php

/**
 * Class and Function List:
 * Function list:
 * - initialize()
 * - getMenuType()
 * - getAdds()
 * - getCurlResponse()
 * - checkLogin()
 * - priorityLinkOption()
 * - signInSecurity()
 * - getProfile()
 * Classes list:
 * - ControllerBase extends \
 */
namespace Qbon\Frontend\Controllers;

use Phalcon\Tag, Qbon\Models\Adds, Qbon\Models\Curl;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    private $memberControllerArray = array(
        "wallet",
        "profile",
        "password"
    );
    protected function initialize()
    {
        
        if (in_array($this->dispatcher->getControllerName() , $this->memberControllerArray)) {
            $this->signInSecurity($this->dispatcher->getControllerName());
        }
        
        $this->priorityLinkOption();
        Tag::setTitle('Album-O-Rama');
    }
    
    protected function getMenuType()
    {
        $menu = array();
        $controllerName = $this->dispatcher->getControllerName();
        $menu[$controllerName] = "current";
        
        $this->view->setVar("menu", $menu);
        $this->view->setVar("ssoUrl", SSO_URL);
        $this->view->setVar("baseUrl", BASE_URL);
    }
    protected function getAdds()
    {
        $addsObj = Adds::find("flag_active='Y'");
        $this->view->setVar("addsObj", $addsObj);
    }
    protected function getCurlResponse($url, $token, $postArray = NULL)
    {
        $curl = new Curl();
        return $curl->getResource($url, $token, $postArray);
    }
    protected function checkLogin()
    {
        $url = API_URL . "api/1.0/account/profile/get";
        if ($token = $this->request->getQuery("token", "string")) {
            $result = $this->getCurlResponse($url, $token);
            if ($result->code == "200") {
                $resultAccountId = $result->member->accountId;
                $resultToken = $result->member->token;
                $resultName = $result->member->name;
                $resultEmail = $result->member->email;
                
                $this->session->set("user_account_id", $resultAccountId);
                $this->session->set("user_token", $resultToken);
                $this->session->set("user_name", $resultName);
                $this->session->set("user_email", $resultEmail);
                $this->getProfile();
                return true;
            } else {
                return header("location:" . SSO_URL . "mobile/login?appKey=" . APP_KEY . "&redirectUrl=" . BASE_URL . "wallet");
            }
        } else {
            if (!$this->session->has("user_account_id") or !$this->session->has("user_token")) {
                return false;
            } else {
                $token = $this->session->get("user_token");
                $result = $this->getCurlResponse($url, $token);
                if ($result->code != "200") {
                    $this->session->remove("user_account_id");
                    $this->session->remove("user_token");
                    $this->session->remove("user_name");
                    $this->session->remove("user_email");
                    return header("location:" . SSO_URL . "mobile/login?appKey=" . APP_KEY . "&redirectUrl=" . BASE_URL . $this->dispatcher->getControllerName());
                }
                $this->getProfile();
                return true;
            }
        }
    }
    protected function priorityLinkOption()
    {
        if ($this->checkLogin()) {
            $this->view->setVar("priorityLinkName", $this->session->get("user_name"));
            $this->view->setVar("priorityLinkLogin", "Y");
        }
    }
    protected function signInSecurity($controllerName = "wallet")
    {
        if (!$this->checkLogin()) {
            return header("location:" . SSO_URL . "mobile/login?appKey=seafe7fea2a25d134a083356d9be30785f8e99a3bb&redirectUrl=" . BASE_URL . $controllerName);
        }
        $edArray = array();
        $edArray["password"] = "";
        $edArray["wallet"] = "";
        $edArray["profile"] = "";
        foreach ($edArray as $key => $value) {
            if ($this->dispatcher->getControllerName() == $key) {
                $edArray[$key] = "ed";
            } else {
                $edArray[$key] = "";
            }
        }
        $this->view->setVar("edArray", $edArray);
    }
    protected function getProfile()
    {
        $this->session->get("user_name") ? $this->view->setVar("userName", $this->session->get('user_name')) : "";
        $this->session->get("user_email") ? $this->view->setVar("userEmail", $this->session->get('user_email')) : "";
    }
}
