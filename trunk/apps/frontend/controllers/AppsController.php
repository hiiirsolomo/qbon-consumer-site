<?php
/**
* Class and Function List:
* Function list:
* - indexAction()
* Classes list:
* - IndexController extends ControllerBase
*/
namespace Qbon\Frontend\Controllers;
use Qbon\Models\Product;
class AppsController extends ControllerBase
{

    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'frontend_how' . $date;
        $this->getMenuType();
        $this->getAdds();
        $countObj = Product::find("flag_active='Y'");
        $newsNumber = count($countObj);
        $page = $this->dispatcher->getParam("p", "int", 1);
        $pageLimit = $page ? ($page - 1) * 10 : 0;
        $appsObj = Product::find(array(
            "conditions" => "flag_active = ?1",
            "bind" => array(
                "1" => "Y"
                ),
            "limit" => array(
                "number" => 10,
                "offset" => $pageLimit
                )
            ));

        $pageNumbers = ceil($newsNumber / 10);
    $this->view->setVar("pageNumbers", $pageNumbers);
        $this->view->setVar("currentPage", $page);
        $this->view->setVar("appsObj", $appsObj);
        $this->view->cache(array(
            "key" => $key
            ));
    }
}