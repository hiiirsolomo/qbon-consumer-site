{{ content() }}
<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
        <div class='title'>最新消息列表</div>
        <div class='pull-right'>
          {{ link_to("news/create", "<i class='icon-pencil'></i>新增", "class": "btn", "style": "margin-bottom:5px")}}
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    創建時間
                  </th>
                  <th>
                    發佈日期
                  </th>
                  <th>
                    標題
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {% for listNews in newsListObj %}

                <tr>
                  <td>{{ listNews.news_id | escape }}</td>
                  <td>{{ listNews.create_time | escape }}</td>
                  <td>{{ listNews.publish_time | escape }}</td>
                  <td>{{ listNews.title | escape }}</td>
                  <td>
                    <div class='text-right'>
                    {{ link_to("news/edit/news_id/" ~ listNews.news_id,
                      "<i class='icon-edit'></i>",
                      "class": "btn btn-success btn-xs")}}
                      {{ link_to("news/delete/news_id/" ~ listNews.news_id,
                      "<i class='icon-trash'></i>",
                      "class": "btn btn-danger btn-xs")}}
                    </div>
                  </td>
                </tr>
                {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>