<div class='row'>
  <div class='col-sm-12'>
    <div class='page-header'>
      <h1 class='pull-left'>
        <i class='icon-table'></i>
        <span>Tables</span>
      </h1>
      <div class='pull-right'>
        <ul class='breadcrumb'>
          <li>
            <a href='index'>
              <i class='icon-bar-chart'></i>
            </a>
          </li>
          <li class='separator'>
            <i class='icon-angle-right'></i>
          </li>
          <li class='active'>Tables</li>
        </ul>
      </div>
    </div>
  </div>
</div>
{{ flashSession.output() }}
{{ content() }}
