{{ content() }}

<div class='row'>
  <div class='col-sm-12'>
    <div class='box bordered-box red-border' style='margin-bottom:0;'>
      <div class='box-header red-background'>
         <div class='title'>apps列表</div>
        <div class='pull-right'>
          {{ link_to("product/create", "<i class='icon-pencil'></i>新增", "class": "btn", "style": "margin-bottom:5px")}}
        </div>
      </div>
      <div class='box-content box-no-padding'>
        <div class='responsive-table'>
          <div class='scrollable-area'>
            <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
              <thead>
                <tr>
                  <th>
                    流水號
                  </th>
                  <th>
                    Apple link
                  </th>
                  <th>
                    Android link
                  </th>
                  <th>
                    名稱
                  </th>
                  <th>
                    圖片
                  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {% for productObj in productRowSetObj %}

                <tr>
                  <td>{{ productObj.product_id | escape }}</td>
                  <td>{{ productObj.apple_link | escape }}</td>
                  <td>{{ productObj.android_link | escape }}</td>
                  <td>{{ productObj.name | escape }}</td>
                  <td>{{ image(productObj.img | stripslashes, "width": "100") }}</td>
                  <td>
                    <div class='text-right'>
                      {{ link_to("product/edit/product_id/" ~ productObj.product_id,
                                 "<i class='icon-edit'></i>",
                                 "class": "btn btn-success btn-xs")}}
                      {{ link_to("product/delete/product_id/" ~ productObj.product_id,
                                 "<i class='icon-trash'></i>",
                                 "class": "btn btn-danger btn-xs")}}
                    </div>
                  </td>
                </tr>
                {% endfor %}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>