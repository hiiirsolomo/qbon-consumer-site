{{ content() }}
<div class='row'>
  <div class='col-sm-12'>
    <div class='box'>
      <div class='box-header red-background'>
        <div class='title'>新增</div>
      </div>
      <div class='box-content'>


        {{ form("adds/update", "method": "post", "class": "form form-horizontal validate-form", "style": "margin-bottom: 0;", "enctype": "multipart/form-data") }}
        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='adds_id'>流水號</label>
          <div class='col-sm-4 controls'>
            {{ addsObj.adds_id }}
            <input value='{{ addsObj.adds_id }}' class='form-control' data-rule-required='true' id='adds_id' name='adds_id' placeholder='分類名' type='hidden'>
          </div>
        </div>

        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='adds-link'>外連網址</label>
          <div class='col-sm-6 controls'>
            <input value='{{ addsObj.apple_link }}' class='form-control' data-rule-required='true' id='adds-link' name='link' placeholder='分類名' type='text'>
          </div>
        </div>

        <div class='form-group'>
          <label class='control-label col-sm-3 col-sm-3' for='adds-img'>產品圖</label>
          <div class='col-sm-4 controls'>
            {% if addsObj.img %}
            {{ image(addsObj.img, "width": "100") }}
            {% endif %}
            <input title='選擇產品圖' type='file' name="img">
          </div>
        </div>

        <div class='form-actions' style='margin-bottom:0'>
          <div class='row'>
            <div class='col-sm-9 col-sm-offset-3'>
              <button class='btn btn-success' type='submit'>
                <i class='icon-save'></i>
                儲存
              </button>
            </div>
          </div>
        </div>
      </form>
      {{ end_form }}
    </div>
  </div>
</div>
</div>