<?php

namespace Qbon\Backend;

class Module
{

	public function registerAutoloaders()
	{
		$loader = new \Phalcon\Loader();

		$loader->registerNamespaces(array(
			'Qbon\Backend\Controllers' => __DIR__.'/controllers/'
		));

		$loader->register();
	}

	public function registerServices($di)
	{
		$di->set('view', function() {

			$view = new \Phalcon\Mvc\View();

			$view->setViewsDir(__DIR__.'/views/');

			$view->setTemplateBefore('main');

			$view->registerEngines(array(
				".volt" => function($view, $di) {

					$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);

					$volt->setOptions(array(
						"compiledPath" => __DIR__."/../../var/volt/",
						"compiledExtension" => ".php"
					));

					return $volt;
				}
			));

			return $view;

		});

		// module session 拆分不共用
		$di->set("session", function() use($di) {
			$router = $di->get('router');
			$session = new \Phalcon\Session\Adapter\Files(array('uniqueId' => $router->getModuleName()));
			$session->start();
			return $session;
		});



		$dispatcher = $di->get('dispatcher');
		$di->set('dispatcher', function() use ($di, $dispatcher) {

			$eventsManager = $di->getShared('eventsManager');

			$auth = new \Auth($di);

			/**
			 * We listen for events in the dispatcher using the Auth plugin
			 */
			$eventsManager->attach('dispatch', $auth);

			// $dispatcher = new \Phalcon\Mvc\Dispatcher();
			$dispatcher->setEventsManager($eventsManager);

			return $dispatcher;
		});

		// The URL component is used to generate all kind of urls in the application
		$di->set("url", function() {
			$url = new \Phalcon\Mvc\Url();
			$url->setBaseUri("/admin/"); // If the project is in the Document Root folder, setBaseUri to '/'
			$url->setStaticBaseUri("/"); // If the project is in the Document Root folder, setBaseUri to '/'
			return $url;
		});

		// Register a user component
		$di->set("elements", function(){
			return new \Elements();
		});
	}

}
