<?php
/**
* Class and Function List:
* Function list:
* - indexAction()
* - listAction()
* - createAction()
* - editAction()
* - updateAction()
* - deleteAction()
* Classes list:
* - ProductController extends ControllerBase
*/
namespace Qbon\Backend\Controllers;

use Qbon\Models\Product, Mike\Util;;

class ProductController extends ControllerBase
{
    
    public function indexAction()
    {
        $this->response->redirect("product/list");
    }
    
    public function listAction($productPropertyCategoryId = null, $productDifferenceCategoryId = null)
    {
        $flagActive = $this->request->getQuery("flag_active") != "N" ? "Y" : "N";
       
        $key = 'product_list' . "_flag_active" . $flagActive;
        
        $exists = $this->view->getCache()->exists($key);
        
        // var_dump($exists); exit;
        if (!$exists) {
            if ($flagActive) {
                $conditionsColumnValueAry["flag_active"] = $flagActive;
            }

            $productRowSetObj = Product::find("flag_active='Y'");
            
            $this->view->setVar("productRowSetObj", $productRowSetObj);
            
        }
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function createAction()
    {
        
        $key = 'product_create';
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function editAction($productId)
    {
        
        $key = 'product_edit' . $productId;
        
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            
            $productObj = Product::findFirst(array(
                "conditions" => "product_id = :product_id:",
                "bind" => array(
                    "product_id" => $productId
                )
            ));
            if ($productObj === false) {
                return $this->response->redirect("product/list");
            }
            
            $this->view->setVar('productObj', $productObj);
        }
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function updateAction()
    {
        if (!$this->request->isPost()) {
            $this->flash->error("錯誤請求");
            return $this->forward("product/list");
        }
        
        $param["product_id"] = $this->request->getPost("product_id", "int");
        $param["apple_link"] = $this->request->getPost("apple_link", "striptags");
        $param["android_link"] = $this->request->getPost("android_link", "striptags");
        $param["name"] = $this->request->getPost("name", "striptags");
        $param["desc"] = $this->request->getPost("desc", "striptags");
        
        
        // create_time  為 default 值
        if (empty($param["product_id"])) {
            $productObj = new Product();
        } else {
            $productObj = Product::findFirst(array(
                "conditions" => "product_id = :product_id:",
                "bind" => array(
                    "product_id" => $param["product_id"]
                )
            ));
        }
        
        $productObj->name = $param["name"];
        $productObj->apple_link = $param["apple_link"];
        $productObj->android_link = $param["android_link"];
        $productObj->update_time = date("Y-m-d H:i:s");
        $productObj->desc = $param["desc"];
        
        $result = $productObj->save();
        
        if ($result === false) {
            $this->flash->error("資料庫儲存錯誤");
            return $this->forward("product/edit");
        }
        
        if ($this->request->hasFiles() == true) {
            $photoDir = UPLOAD_IMG_ROOT . "product/" . substr(md5($productObj->product_id) , 0, 8) . "/";
            $photoUrlDir = UPLOAD_IMG_PATH . "product/" . substr(md5($productObj->product_id) , 0, 8) . "/";
            
            $photoName = $_FILES["img"]["name"];
            
            $nameCount = 0;
            while (file_exists($photoDir . $photoName)) {
                $nameCount++;
                $photoName = $nameCount . "_" . $_FILES["img"]["name"];
            }
            Util::checkDir($photoDir);
            
            $photoPath = $photoDir . $photoName;
            $photoUrlPath = $photoUrlDir . $photoName;
            $moveUploadSucc = move_uploaded_file($_FILES["img"]["tmp_name"], $photoPath);
            
            if ($uploadSucc === false) {
                $this->flash->error("檔案傳輸錯誤");
                return $this->forward("product/edit");
            }
            
            $productObj->img = $photoUrlPath;
            $result = $productObj->save();
            
            if ($result === false) {
                $this->flash->error("資料庫儲存錯誤");
                return $this->forward("product/edit");
            }
        }
        
        $this->flash->success("更新成功");
        return $this->forward("product/list");
    }
    
    public function deleteAction($productId)
    {
        $param["product_id"] = $this->request->getQuery("product_id", "int");
        
        $productObj = Product::findFirst(array(
            "conditions" => "product_id = :product_id:",
            "bind" => array(
                "product_id" => $productId
            )
        ));
        if ($productObj === false) {
            return $this->response->redirect("product/list");
        }
        
        $productObj->flag_active = "N";
        $result = $productObj->save();
        
        if ($result === false) {
            $this->flash->error("資料庫儲存錯誤");
            return $this->forward("product/edit");
        }
        
        $this->flash->success("更新成功");
        return $this->forward("product/list");
    }
}
