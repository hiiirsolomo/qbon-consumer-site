<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * - listAction()
 * - createAction()
 * - updateAction()
 * - editAction()
 * Classes list:
 * - NewsController extends ControllerBase
 */
namespace Qbon\Backend\Controllers;
use Qbon\Models\News;
class NewsController extends ControllerBase
{
    public function indexAction()
    {
        $this->redirect("news/list");
    }
    public function listAction()
    {
        $date = date("Y-m-d");
        $key = 'admin_news_list' . $date;
        
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            $listNewsObj = News::find(array(
                "flag_active = 'Y'",
                "order" => "create_time DESC"
            ));
            $this->view->setVar("newsListObj", $listNewsObj);
        }
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function createAction()
    {
        $date = date("Y-m-d");
        $key = 'admin_news_create' . $date;
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function updateAction()
    {
        if (!$this->request->isPost()) {
            $this->flash->error("錯誤請求");
            return $this->redirect("news/list");
        }
        $date = date("Y-m-d");
        $key = 'admin_news_update' . $date;
        $exists = $this->view->getCache()->exists($key);

        if (!$exists) {

            $params = array();
            $params["news_id"] = $this->request->getPost("news_id", "int", "");
            $params["title"] = $this->request->getPost("title", "striptags");
            $params["content"] = $this->request->getPost("content");
            $params["publish_time"] = $this->request->getPost("publish_time", "string");

            if (!$params["news_id"]) {
                unset($params["news_id"]);
                $newsObj = new News();
            } else {
                $newsObj = News::findFirst(array(
                    "news_id='{$params["news_id"]}'"
                ));
            }

          foreach ($params as $key => $value) {
                $newsObj->$key = $value;
          }


            if ($newsObj->save() == false) {
                $this->flash->error("資料庫儲存錯誤");
                return $this->redirect("news/list");
            }
            $this->flash->success("更新成功");
            return $this->redirect("news/list");
        }
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function editAction()
    {
        $params = array();
        if ($params["news_id"] = $this->dispatcher->getParam("news_id","int","")) {
            $newsObj = News::findFirst(array(
                "news_id='{$params["news_id"]}'"
            ));
        } else {
            $this->flash->error("操作失敗。");
            return $this->redirect("news/list");
        }
        $this->view->pick("news/create");
        $this->view->setVar("newsObj",$newsObj);
    }
    public function deleteAction()
    {
        $params = array();
        if($params["news_id"] = $this->dispatcher->getParam("news_id","int","")) {
            $newsObj = News::findFirst(array(
                "news_id='{$params["news_id"]}'"
                ));
            $newsObj->flag_active = "N";
            if(!$newsObj->save()){
                $this->flash->error("操作失敗。");
                return $this->redirect("news/list");
            }
        } else {
            $this->flash->error("操作失敗。");
            return $this->redirect("news/list");
        }
        $this->flash->success("更新成功");
        return $this->redirect("news/list");
    }
}
