<?php
/**
* Class and Function List:
* Function list:
* - indexAction()
* - listAction()
* - createAction()
* - editAction()
* - updateAction()
* - deleteAction()
* Classes list:
* - ProductController extends ControllerBase
*/
namespace Qbon\Backend\Controllers;

use Qbon\Models\Adds, Mike\Util;;

class AddsController extends ControllerBase
{
    
    public function indexAction()
    {
        $this->response->redirect("adds/list");
    }
    
    public function listAction()
    {
        $flagActive = $this->request->getQuery("flag_active") != "N" ? "Y" : "N";
       
        $key = 'adds_list' . "_flag_active" . $flagActive;
        
        $exists = $this->view->getCache()->exists($key);
        
        // var_dump($exists); exit;
        if (!$exists) {
            if ($flagActive) {
                $conditionsColumnValueAry["flag_active"] = $flagActive;
            }

            $addsRowSetObj = Adds::find("flag_active='Y'");
            
            $this->view->setVar("addsRowSetObj", $addsRowSetObj);
            
        }
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function createAction()
    {
        
        $key = 'adds_create';
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function editAction($addsId)
    {
        
        $key = 'adds_edit' . $addsId;
        
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            
            $addsObj = Adds::findFirst(array(
                "conditions" => "adds_id = :adds_id:",
                "bind" => array(
                    "adds_id" => $addsId
                )
            ));
            if ($addsObj === false) {
                return $this->response->redirect("adds/list");
            }
            
            $this->view->setVar('addsObj', $addsObj);
        }
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
    
    public function updateAction()
    {
        if (!$this->request->isPost()) {
            $this->flash->error("錯誤請求");
            return $this->forward("adds/list");
        }
        
        $param["adds_id"] = $this->request->getPost("adds_id", "int");
        $param["link"] = $this->request->getPost("link", "striptags");
        
        
        // create_time  為 default 值
        if (empty($param["adds_id"])) {
            $addsObj = new Adds();
        } else {
            $addsObj = Adds::findFirst(array(
                "conditions" => "adds_id = :adds_id:",
                "bind" => array(
                    "adds_id" => $param["adds_id"]
                )
            ));
        }
        
        $addsObj->name = $param["name"];
        $addsObj->link = $param["link"];
        $addsObj->update_time = date("Y-m-d H:i:s");
        
        $result = $addsObj->save();
        
        if ($result === false) {
            $this->flash->error("資料庫儲存錯誤");
            return $this->forward("adds/edit");
        }
        
        if ($this->request->hasFiles() == true) {
            $photoDir = UPLOAD_IMG_ROOT . "adds/" . substr(md5($addsObj->adds_id) , 0, 8) . "/";
            $photoUrlDir = UPLOAD_IMG_PATH . "adds/" . substr(md5($addsObj->adds_id) , 0, 8) . "/";
            
            $photoName = $_FILES["img"]["name"];
            
            $nameCount = 0;
            while (file_exists($photoDir . $photoName)) {
                $nameCount++;
                $photoName = $nameCount . "_" . $_FILES["img"]["name"];
            }
            Util::checkDir($photoDir);
            
            $photoPath = $photoDir . $photoName;
            $photoUrlPath = $photoUrlDir . $photoName;
            $moveUploadSucc = move_uploaded_file($_FILES["img"]["tmp_name"], $photoPath);
            
            if ($uploadSucc === false) {
                $this->flash->error("檔案傳輸錯誤");
                return $this->forward("adds/edit");
            }
            
            $addsObj->img = $photoUrlPath;
            $result = $addsObj->save();
            
            if ($result === false) {
                $this->flash->error("資料庫儲存錯誤");
                return $this->forward("adds/edit");
            }
        }
        
        $this->flash->success("更新成功");
        return $this->forward("adds/list");
    }
    
    public function deleteAction($addsId)
    {
        $param["adds_id"] = $this->request->getQuery("adds_id", "int");
        
        $addsObj = Adds::findFirst(array(
            "conditions" => "adds_id = :adds_id:",
            "bind" => array(
                "adds_id" => $addsId
            )
        ));
        if ($addsObj === false) {
            return $this->response->redirect("adds/list");
        }
        
        $addsObj->flag_active = "N";
        $result = $addsObj->save();
        
        if ($result === false) {
            $this->flash->error("資料庫儲存錯誤");
            return $this->forward("adds/edit");
        }
        
        $this->flash->success("更新成功");
        return $this->forward("adds/list");
    }
}
