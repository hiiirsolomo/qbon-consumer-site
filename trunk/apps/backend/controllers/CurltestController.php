<?php
/**
* Class and Function List:
* Function list:
* - indexAction()
* - getResource()
* Classes list:
* - CurltestController extends ControllerBase
*/
namespace Qbon\Backend\Controllers;

class CurltestController extends ControllerBase
{
    public function indexAction()
    {
        
        $url = API_URL.'api/1.0/account/profile/get';
        echo $res = $this->getResource($url);
    }
    
    protected function getResource($url)
    {
        $ch = curl_init();
        //永遠抓最新
        $header[] = "Cache-Control: no-cache";
        $header[] = "Pragma: no-cache";
        $header[] = "APPLICATION-KEY: dbc199de6f321a363ab2eeb29a38c92a";
        $header[] = "AUTHORIZATION: B7EY8iMwyIMgRwF8BzfRNpswr86oJ111JTy01nHvkjN3wCilQH4JB2fJYYYS8_PaJmKHtRH3CFucY7GiANgt2yoDAiWiR3LTgpJNZIVgHwZ1qSBIIlATwpEoYXsarq02YqxAKvLKN";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        //等待時間
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        //Post Data
        // $postData = "accountId=" . "2760" . "&redeemed=" . "1";
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        // 設定referer
        curl_setopt($ch, CURLOPT_REFERER, API_URL);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($result);
        print_r($obj);
        echo $obj->code;
    }
}
