<?php

namespace Qbon\Backend\Controllers;

use Phalcon\Tag;

class ControllerBase extends \Phalcon\Mvc\Controller
{

	protected function initialize()
    {
        // Phalcon\Tag::prependTitle('INVO | ');
    }

    protected function forward($uri){
        $uriParts = explode('/', $uri);
        return $this->dispatcher->forward(
            array(
                'controller' => $uriParts[0],
                'action' => $uriParts[1]
            )
        );
    }

    protected function redirect($uri)
    {
        return $this->response->redirect($uri);
    }

    protected function getParam($name, $filter = null, $defaultValue = null)
    {
        return $this->dispatcher->getParam($name, $filter, $defaultValue) ?
                    $this->dispatcher->getParam($name, $filter, $defaultValue) :
                    $this->request->getQuery($name, $filter, $defaultValue);
    }

    protected function getParams()
    {
        return array_merge($this->request->getQuery(), $this->request->getPost(), $this->dispatcher->getParams());
    }

    // 取 $_GET 和 URL 參數合併，
    // getQuery 為 dispatcher 和 request 取參數合併
    // dispatcher->getParam 的 key 為 數字時，array_merge 會不對
    protected function getQuery($name = null, $filter = null, $defaultValue = null)
    {
        if (isset($name)) {
            return $this->dispatcher->getParam($name, $filter, $defaultValue) ?
                    $this->dispatcher->getParam($name, $filter, $defaultValue) :
                    $this->request->getQuery($name, $filter, $defaultValue);
        } else {
            return array_merge($this->request->getQuery(), $this->dispatcher->getParams());
        }

    }
}