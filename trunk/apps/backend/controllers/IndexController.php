<?php

namespace Qbon\Backend\Controllers;

use Qbon\Models\Admin;

class IndexController extends ControllerBase
{

	public function indexAction()
	{
        $this->flash->success('Welcome ' . $adminRowObj->account);
		// $key = 'artist'.$artistId;

  //       $exists = $this->view->getCache()->exists($key);
  //       if (!$exists) {

  //       }

  //       $this->view->cache(array("key" => $key));

	}



    // 各大網站趨勢使用 signin 取代 login
    public function signinAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        // 非post return
        if (! $this->request->isPost()) {
            return;
        }

        $param["account"] = $this->request->getPost('account', 'email');
        $param["password"] = $this->request->getPost('password');
        $adminRowObj = Admin::findFirst(array( "conditions" => "account = :account: AND flag_active = 'Y'",
                                               "bind" => array( "account" => $param["account"] )));

        if ($adminRowObj === false) {
            return $this->flash->error('Wrong account');
        }

        // 密碼錯誤 return
        if (! $this->checkPassword($param["password"], $adminRowObj->hash_password)) {
            return $this->flash->error('Wrong account/password');
        }

        // 登入成功
        $this->registerSession($adminRowObj);

        return $this->response->redirect("index/index");
    }




    // post/redirect/get 待之後全改為 ajax 解
    public function signupAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        // 非post return
        if (! $this->request->isPost()) {
            return;
        }

        $param["account"] = $this->request->getPost('account', 'email');
        $param["password"] = $this->request->getPost('password');
        $param["password_confirmation"] = $this->request->getPost('password_confirmation');

        if ($param["password"] != $param["password_confirmation"]) {
            return $this->flash->error("密碼不一致");
        }

        $adminRowObj = Admin::findFirst(array( "conditions" => "account = :account: AND flag_active = 'Y'",
                                         "bind" => array( "account" => $param["account"] )));

        if ($adminRowObj !== false) {
            return $this->flash->error("信箱已註冊");
        }

        $adminRowObj = new Admin();
        $adminRowObj->account = $param["account"];
        $adminRowObj->hash_password = $this->hashPassword($param["password"]);
        $adminRowObj->update_time = date("Y-m-d H:i:s");
        $adminRowObj->create_time = date("Y-m-d H:i:s");
        $result = $adminRowObj->create();

        if ($result === false) {
            return $this->flash->error("資料庫儲存錯誤");
        }

        // 註冊成功
        $this->registerSession($adminRowObj);
        $this->flash->success('Welcome ' . $adminRowObj->account);
        return $this->forward("index/index");


    }

    public function signoutAction()
    {
        $this->session->remove("auth");
        return $this->forward("index/signin");
    }

    /**
     * Register authenticated admin into session data
     *
     * @param Admin $admin
     */
    private function registerSession($admin)
    {
        $this->session->set('auth', array(
            'admin_id' => $admin->admin_id,
            'account' => $admin->account
        ));
    }

    // 加密密碼核對
    private function checkPassword($password, $hashPassword)
    {
        $pwdHasher = new \PasswordHash(8, FALSE);
        return $pwdHasher->CheckPassword($password, $hashPassword);
    }

    private function hashPassword($password)
    {
        $pwdHasher = new \PasswordHash(8, FALSE);
        return $pwdHasher->HashPassword($password);
    }

}

