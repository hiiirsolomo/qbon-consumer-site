<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - WalletController extends ControllerBase
 */
namespace Qbon\Mobile\Controllers;
class WalletController extends ControllerBase
{

    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_wallet' . $date;
        $this->getMenuType();

        $url = API_URL."api/dev0.1/consumer/wallet/get";
        $redeemed = $this->dispatcher->getParam("redeemed", "int", "0");
        $accountId = $this->session->get("user_account_id");
        $postArray = array(
            "redeemed" => $redeemed,
            "accountId" => $accountId
        );
        $result = $this->getCurlResponse($url, $this->session->get("user_token") , $postArray);
        $walletObj = $result->items;
        $this->view->setVar("redeemed" . $redeemed, "current");
        $this->view->setVar("walletObj", $result->items);
        $this->view->cache(array(
            "key" => $key
        ));
    }
}

