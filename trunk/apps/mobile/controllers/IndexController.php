<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - IndexController extends ControllerBase
 */
namespace Qbon\Mobile\Controllers;
use Qbon\Models\News;
use Qbon\Models\Product;
class IndexController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_index' . $date;
        
        $exists = $this->view->getCache()->exists($key);
        if (!$exists) {
            $this->getMenuType();
            $newsObj = News::find(array(
                "conditions" => "flag_active = ?1",
                "bind" => array(
                    "1" => "Y"
                ) ,
                "order" => "publish_time DESC"
            ));
            
            //Detect special conditions devices
            $iPod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
            $iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $iPad = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            $android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
            
            if($iPod || $iPhone || $iPad){
            	$appsObj = Product::find(array("conditions"=>"flag_active = ?1 AND apple_link != ?2","bind"=>array("1"=>"Y", "2"=>"")));
            	$this->view->setVar("platfrom", "iOs");
            }else if($android){
            	$appsObj = Product::find(array("conditions"=>"flag_active = ?1 AND android_link != ?2","bind"=>array("1"=>"Y", "2"=>"")));
            	$this->view->setVar("platfrom", "android");            	            	 
            }            
                       
            $this->getAdds();
            $this->view->setVar("appsObj", $appsObj);
            $this->view->setVar("bannerOp", "On");
            $this->view->setVar("newsObj", $newsObj);
        }
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function signoutAction()
    {
        $this->session->remove("user_account_id");
        $this->session->remove("user_token");
        $this->session->remove("user_name");
        return $this->response->redirect("mobile/");
    }
}

