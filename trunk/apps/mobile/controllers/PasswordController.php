<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - IndexController extends ControllerBase
 */
namespace Qbon\Mobile\Controllers;
class PasswordController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_profile' . $date;
        $this->getMenuType();
        $url = API_URL."api/1.0/account/profile/get";
        $result = $this->getCurlResponse($url,$this->session->get("user_token"));
        $this->view->setVar("memberObj",$result->member);
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function updateAction()
    {  
        if (!$this->request->isPost()) {
            echo <<<END
            <script>
                alert("操作錯誤。");
                history.back(1);
            </script>
END;
        }
        $postArray = array(
            "originalPassword" => $this->request->getPost("originalPassword","alphanum"),
            "newPassword" => $this->request->getPost("newPassword","alphanum"),
            );
        $url = API_URL."api/1.0/account/changePassword";
        $result = $this->getCurlResponse($url,$this->session->get("user_token"),$postArray);
        if($result->code == "200"){
            echo <<<END
            <script>
                alert("修改成功。");
                history.back(1);
            </script>
END;
        } else {
            echo <<<END
            <script>
                alert("修改失敗。");
                history.back(1);
            </script>
END;
        }
    }
}

