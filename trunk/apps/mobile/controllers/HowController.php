<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * Classes list:
 * - IndexController extends ControllerBase
 */
namespace Qbon\Mobile\Controllers;
class HowController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_how' . $date;
        $this->getMenuType();
        
        $this->view->cache(array(
            "key" => $key
        ));
    }
}

