<?php
/**
 * Class and Function List:
 * Function list:
 * - indexAction()
 * - updateAction()
 * Classes list:
 * - ProfileController extends ControllerBase
 */
namespace Qbon\Mobile\Controllers;
class ProfileController extends ControllerBase
{
    
    public function indexAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_profile' . $date;
        $this->getMenuType();
        $url = API_URL . "api/1.0/account/profile/get";
        $result = $this->getCurlResponse($url, $this->session->get("user_token"));
        $this->view->setVar("memberObj", $result->member);
        $this->view->cache(array(
            "key" => $key
        ));
    }
    public function updateAction()
    {
        $date = date("Y-m-d");
        $key = 'mobile_profile_update' . $date;
        $param = array();
        $param["name"] = $this->request->getPost("name", "striptags");
        $param["gender"] = $this->request->getPost("gender", "striptags");
        $param["zipCode"] = $this->request->getPost("zipCode", "int");
        // $param["email"] = $this->request->getPost("email", "email");
        // $param["mobile"] = $this->request->getPost("mobile", "int");
        $url = API_URL . "api/1.0/account/profile/update";
        $result = $this->getCurlResponse($url, $this->session->get("user_token") , $param);

        if ($result->code == "200") {
            echo <<<END
            <script>
                alert("修改成功。");
                history.back(1);
            </script>
END;
            
        } else {
            echo <<<END
            <script>
                alert("修改失敗。");
                history.back(1);
            </script>
END;
            
        }
    }
}

