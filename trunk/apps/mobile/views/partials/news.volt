{% if newsObj %}
<article id="top-news">
    <div id="grid-lists">
        {% for newsRow in newsObj %}
        <div class="gird">
            <h3>{{newsRow.publish_time}}</h3>
            <p>
                {{newsRow.content}}
            </p>
        </div>
        {% endfor %}
    </div>
    <!---
    <div class="footer">
        <a href="" class="btn-pink">更多</a>
    </div>
    -->
</article>
{% endif %}
