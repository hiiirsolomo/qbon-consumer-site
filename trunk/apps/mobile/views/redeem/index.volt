<div id="content">
    
                <div class="tutorial-box redeem-1">
                    <span>
                        Step 1.
                    </span>
                    <h3>
                        於聯盟app中開啟Qbon優惠牆或下載QbonWallet
                    </h3>
                    <img src="/mobilepublic/images/img_tutorial_redeem1.png" alt="">
                </div>
                <div class="tutorial-box redeem-2">
                    <span>
                        Step 2.
                    </span>
                    <h3>
                        登入後點選『我的口袋』，找到想要兌換的那張票券
                    </h3>
                    <img src="/mobilepublic/images/img_tutorial_redeem2.png" alt="">
                </div>
                <div class="tutorial-box redeem-3">
                    <span>
                        Step 3.
                    </span>
                    <h3>
                        由店家確認點擊『馬上兌換』
                    </h3>
                    <img src="/mobilepublic/images/img_tutorial_redeem3.png" alt="">
                </div>
            </div>
