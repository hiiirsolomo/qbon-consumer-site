<div id="nav-sub-content">
    <div class="tab-1 {{ redeemed0 }}">
        <a href="/mobile/wallet/index/redeemed/0">未兌換</a>
    </div>
    <div class="tab-2 {{ redeemed1 }}">
        <a href="/mobile/wallet/index/redeemed/1">已兌換</a>
    </div>
</div>

<div id="content">

    <div id="grid-lists" class="mywallet">
        {% if walletObj %}
        {% for walletRow in walletObj %}
        <div class="gird">
            <div class="gird-img">
                <img src="{{ walletRow.image }}" width="115" height="70" alt="">
            </div>
            <div class="gird-data">
                <h3><a href="">{{ walletRow.offerName }}</a></h3>
                <p>{{ walletRow.detail }}</p>
                <div class="footer">
                    {% if walletRow.offerType == "COUPON" %}
                    <img src="/mobilepublic/images/icon_coupon.png" alt="">
                    優惠券
                    {% elseif walletRow.offerType == "MISSION" %}
                    <img src="/mobilepublic/images/icon_mission.png" alt="">
                    任務
                    {% elseif walletRow.offerType == "VOUCHER" %}
                    <img src="/mobilepublic/images/icon_voucher.png" alt="">
                    商品劵
                    {% endif %}
                </div>
            </div>
        </div>
        {% endfor %}
        {% else %}
         目前無資料
        {% endif %}

    </div>
</div>