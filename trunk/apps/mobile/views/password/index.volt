<div id="content">
    <script> 
        function submitform() {
            var txt = $("#text1").val();
            var txt2 = $("#text2").val();
            var txt3 = $("#text").val();
            re = /[a-zA-Z0-9]/;
            if(txt3 == ""){
                alert("請輸入原密碼哦！");
            }
            if(!re.test(txt)){
             alert("有非英文及數字的字喔!"); 
         }else if(txt !== txt2){
            alert("確認密碼不同噢！");
        }else{
            $("#pwform").submit();
        }

    } 
</script> 
<div class="from-box">
   <form id="pwform" action="{{ url("mobile/password/update") }}" method="post">
    <table class="from-table">
        <tr>
            <td class="from-tdTitle">
                舊密碼
            </td>
            <td>
                <input type="password" name="originalPassword" id="text" value="" class="from-inputText">
            </td>
        </tr>

        <tr>
            <td class="from-tdTitle">
                新密碼
            </td>
            <td>
                <input type="password" id="text1" name="newPassword" value="" class="from-inputText">
            </td>
        </tr>

        <tr>
            <td class="from-tdTitle">
                再次輸入
            </td>
            <td>
                <input type="password" id="text2" name="newPassword2" value="" class="from-inputText">
            </td>
        </tr>
    </table>
    <a href="javascript: submitform()" class="btn-pink">確認</a>
    <a href="javascript: history.back(1)" class="btn-gray">取消</a>
</form>
</div>
</div>