<div id="content">
    <div id="grid-lists">
    {% if appsObj %}
    {% for appsRow in appsObj %}
        <div class="gird">
            <div class="gird-img">
                <img src="{{ url(appsRow.img) }}" width="70" height="70" alt="">
            </div>
            <div class="gird-data">
            <h3><a href="">{{ appsRow.name }}</a></h3>
                <p>{{ appsRow.desc|nl2br }}</p>
            </div>
            {% if platfrom == "iOs" %}
                <a href="{{ appsRow.apple_link }}" class="btn-pink download"></a>            
            {% elseif platfrom == "android" %}
                <a href="{{ appsRow.android_link }}" class="btn-pink download"></a>                            
            {% endif %}

        </div>
    {% endfor %}
    {% endif %}

<!--        <a href="" class="btn-dark">載入更多</a>-->
    </div>
</div>
