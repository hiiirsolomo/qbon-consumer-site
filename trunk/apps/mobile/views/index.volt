<!DOCTYPE html>
<html lang="zh">
<head>
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta charset="utf-8" />
    <title>Qbon 優惠牆</title>
    <meta name="keywords" content="">
    <meta name="description" content=""/>
    <link rel="stylesheet" type="text/css" href="/mobilepublic/stylesheets/reset.css" />
    <link rel="stylesheet" type="text/css" href="/mobilepublic/stylesheets/common-use.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="/js/jquery.twzipcode.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#btn-menu").click(function() {
                $(this).toggleClass("current");
                $("#nav-menu").slideToggle();
            });
        });
    </script>
</head>
<body>
    <header>
        {{ partial('partials/priorityLink')}}
    </header>

    <section>
        {{ partial('partials/banner') }}
        <article id="main">
        
            {{ partial('partials/menu') }}
            

            {{ content() }}
        </article>

        {{ partial('partials/adds')}}

        {{ partial('partials/news') }}
    </section>

     <footer>
        <div class="footer-desktop">
            <a href="">前往電腦版 Desktop Site</a>
        </div>
        Copyright © 2014 Hiiir.com.<br>
        All Rights Reserved
    </footer>
</body>
</html>