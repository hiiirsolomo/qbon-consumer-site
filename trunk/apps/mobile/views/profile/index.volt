<script>
    $(function(){
        $('#twzipcode').twzipcode({
            {% if memberObj.zipCode %}
            zipcodeSel: {{ memberObj.zipcode }},
            readonly: false
            {% endif %}
        });
    });
    function submitform() {
        $("#pfform").submit();
    } 
</script>
{% if memberObj.gender == "female" %}
{% set female = "checked" %}
{% else %}
{% set male = "checked" %}
{% endif %}
<div id="content">
    <div class="from-box">
        <form id="pfform"  action="{{ url("mobile/profile/update") }}" method="post">
            <table class="from-table">
                <tr>
                    <td class="from-tdTitle">
                        姓名
                    </td>
                    <td>
                        <input type="text" name="name" class="from-inputText" value="{{ memberObj.name }}">
                    </td>
                </tr>

                <tr>
                    <td class="from-tdTitle">
                        性別
                    </td>
                    <td>
                        <input type="radio" name="gender" value="male" {{ male }}>男　<input type="radio" name="gender" value="female" {{ female }}>女
                    </td>
                </tr>

                <tr>
                    <td class="from-tdTitle">
                        地址
                    </td>
                    <td>
                        <div id="twzipcode">
                          <div data-role="zipcode" style="display: none"></div>  <!--自訂郵遞區號容器，以及套用 .zipcode 樣式-->
                          <div data-role="county" style="display: inline"></div> <!--自訂縣市選單容器，以及套用 .county 樣式-->
                          <div data-role="district" style="display: inline"></div> <!--自訂鄉鎮市區選單容器，以及套用 .district 樣式-->
                      </div>
                  </td>
              </tr>

              <tr>
                <td class="from-tdTitle">
                    Email
                </td>
                <td>
                <input type="text" name="email" value="{{ memberObj.email }}" class="from-inputText" disabled>
                </td>
            </tr>

            <tr>
                <td class="from-tdTitle">
                    行動電話
                </td>
                <td>
                    <input type="text" name="mobile" value="{{ memberObj.mobile }}" class="from-inputText" disabled>
                </td>
            </tr>
        </table>
        <a href="javascript:submitform()" class="btn-pink">確認</a>
        <a href="" class="btn-gray">取消</a>
    </div>
</div>