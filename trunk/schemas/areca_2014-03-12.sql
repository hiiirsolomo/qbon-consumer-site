# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.30)
# Database: areca
# Generation Time: 2014-03-12 07:26:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(255) DEFAULT NULL,
  `hash_password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `flag_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`admin_id`, `account`, `hash_password`, `name`, `update_time`, `create_time`, `flag_active`)
VALUES
	(1,'mike_huang@hiiir.com','$2a$08$fDCz0arykSbKxKNq1cmbj.pyI5TaDMo1nrRe1qa1YV9aS7FRu/X4e','密碼:1234','2014-03-09 23:06:18','2014-03-09 23:06:18','Y');

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_auth`;

CREATE TABLE `admin_auth` (
  `auth_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL COMMENT 'admin_id = 0 為 public',
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_auth` WRITE;
/*!40000 ALTER TABLE `admin_auth` DISABLE KEYS */;

INSERT INTO `admin_auth` (`auth_id`, `admin_id`, `controller`, `action`, `create_time`, `update_time`, `sort`)
VALUES
	(1,1,'product','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(2,1,'product','edit','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(3,1,'product','update','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(4,1,'product','delete','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(5,1,'metial','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(6,1,'code','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(7,1,'product','index','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(9,0,'index','signin','2014-03-02 21:19:58','2014-03-02 21:19:58',NULL),
	(10,0,'index','signout','2014-03-02 21:19:58','2014-03-02 21:19:58',NULL),
	(11,0,'index','signup','2014-03-02 21:19:58','2014-03-02 21:19:58',NULL),
	(13,1,'index','index','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(14,1,'product_property_category','index','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(15,1,'product_property_category','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(16,1,'product_property_category','edit','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(17,1,'product_property_category','update','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(18,1,'product_property_category','delete','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(19,1,'product_property_category','create','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(20,1,'product_difference_category','index','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(21,1,'product_difference_category','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(22,1,'product_difference_category','edit','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(23,1,'product_difference_category','update','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(24,1,'product_difference_category','delete','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(25,1,'product_difference_category','create','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(26,1,'product','index','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(27,1,'product','list','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(28,1,'product','edit','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(29,1,'product','update','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(30,1,'product','delete','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL),
	(31,1,'product','create','2014-02-26 21:43:46','2014-02-26 22:20:36',NULL);

/*!40000 ALTER TABLE `admin_auth` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_property_category_id` int(11) DEFAULT NULL,
  `product_difference_category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT '',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `flag_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`product_id`, `product_property_category_id`, `product_difference_category_id`, `name`, `img`, `create_time`, `update_time`, `flag_active`)
VALUES
	(1,1,2,'七星淡煙','var/img/product/d41d8cd9/9_d473151.jpg',NULL,'2014-03-12 15:12:27','Y'),
	(2,5,3,'包裝','var/img/product/d41d8cd9/10_d473151.jpg',NULL,'2014-03-12 15:26:36','Y');

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_difference_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_difference_category`;

CREATE TABLE `product_difference_category` (
  `product_difference_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_property_category_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT '',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `flag_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`product_difference_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_difference_category` WRITE;
/*!40000 ALTER TABLE `product_difference_category` DISABLE KEYS */;

INSERT INTO `product_difference_category` (`product_difference_category_id`, `product_property_category_id`, `name`, `create_time`, `update_time`, `flag_active`)
VALUES
	(1,1,'七星-濃','2014-03-12 07:05:01','2014-03-12 08:50:37','Y'),
	(2,1,'七星-中淡','2014-03-12 07:05:24','2014-03-12 07:05:24','Y'),
	(3,5,'包裝盒','2014-03-12 07:19:32','2014-03-12 07:19:32','Y'),
	(4,2,'茶裏王','2014-03-12 07:19:47','2014-03-12 07:19:47','Y'),
	(5,1,'七星-超淡','2014-03-12 07:20:07','2014-03-12 07:20:07','Y');

/*!40000 ALTER TABLE `product_difference_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_property_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_property_category`;

CREATE TABLE `product_property_category` (
  `product_property_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL,
  `flag_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`product_property_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_property_category` WRITE;
/*!40000 ALTER TABLE `product_property_category` DISABLE KEYS */;

INSERT INTO `product_property_category` (`product_property_category_id`, `name`, `create_time`, `update_time`, `flag_active`)
VALUES
	(1,'煙品2','2014-03-10 04:26:01','2014-03-10 22:07:24','Y'),
	(2,'飲料','2014-03-10 04:26:11','2014-03-10 04:26:11','Y'),
	(3,'test','2014-03-10 21:33:47','2014-03-10 21:33:47','N'),
	(4,'檳榔','2014-03-10 22:06:28','2014-03-10 22:06:28','Y'),
	(5,'包裝','2014-03-10 22:06:28','2014-03-10 22:06:28','Y');

/*!40000 ALTER TABLE `product_property_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table supplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
